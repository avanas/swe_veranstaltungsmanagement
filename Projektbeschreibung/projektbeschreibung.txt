

Ziel ist die Erstellung eines Organisationssystem im universitären Bereich.
Dieses soll unterschiedlichen Benutzergruppen die Möglichkeit zur Verwaltung,
Auswertung und Anmeldung zu verschiedenen Veranstaltungen bieten.
Es gibt drei verschiedene Arten von Veranstaltungen: Lehrveranstaltungen,
Prüfungen und sonstige Veranstaltungen.
Lehrpersonen haben die Möglichkeit nach Räumen zu suchen, die an einem bestimmten
Zeitpunkt an der Universität verfügbar sind. Sie können dann diesen Raum für eine
Veranstaltung buchen. Sie haben die Möglichkeit die Dauer der Veranstaltung
anzugeben und zwischen einmaligen und wöchentlich stattfindenden Veranstaltungen
zu unterscheiden. Bei Prüfungen besteht außerdem die Möglichkeit für jeden
Teilnehmer eine Note einzutragen.
Studenten können Veranstaltungen anhand des Namens, Ort oder Zeit suchen und
sich dann für eine od. mehrere anmelden. Außerdem soll es für sie möglich sein eine
Übersicht aller Veranstaltungen, für die sie angemeldet sind, in Form eines
Stundenplans einzusehen, bei Prüfungen ist können sie zudem die jeweilige Note einsehen.
Statistiker werten die Belegungen von Veranstaltungen und Räumen aus. Sie haben die
Möglichkeit Daten über die Auslastung von Räumen, die Teilnehmerzahlen einzelner
Kurse und die Notenverteilung bei Prüfungen abzufragen.