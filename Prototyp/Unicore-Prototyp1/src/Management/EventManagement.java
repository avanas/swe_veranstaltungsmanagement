/**
 * 
 */
package Management;

import java.sql.Time;
import java.util.Date;

import Events.Event;

/**
 * @author vmb
 *
 */
public class EventManagement {
	
	/**
	 * 
	 */
	public EventManagement(){
		
	}
	
	/**
	 * @param eventType
	 * @param eventName
	 * @param eventCapacity
	 * @param eventDate
	 * @param eventDiscipline
	 * @param RoomType
	 * @param eventTime
	 * @param eventDuration
	 */
	public void addEvent(String eventType, String eventName, Integer eventCapacity, Date eventDate, String eventDiscipline, String RoomType, Time eventTime, Integer eventDuration){
	
	}
	
	/**
	 * @param event
	 */
	public void deleteEvent(Event event){
		
	}
	
	/**
	 * @param eventType
	 * @param eventName
	 * @param eventDiscipline
	 */
	public void editEvent(String eventType, String eventName, String eventDiscipline){
		
	}
	
	/**
	 * @param eventName
	 * @return
	 */
	public Event getEvent(String eventName){
		return null;
		
	}
	
	/**
	 * @param eventDate
	 * @param eventRoomType
	 */
	public void checkRoom(Date eventDate, String eventRoomType){
		
	}
}
