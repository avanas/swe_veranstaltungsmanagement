/**
 * 
 */
package Rooms;

/**
 * @author vmb
 *
 */
public class Room {
	
	/**
	 * 
	 */
	String roomNr;
	String roomName;
	Integer roomCapacity;
	String roomType;
	String roomBuilding;
	
	/**
	 * @param roomNr
	 * @param roomName
	 * @param roomCapacity
	 * @param roomType
	 * @param roomBuilding
	 */
	public Room(String roomNr, String roomName, Integer roomCapacity, String roomType, String roomBuilding){
		
	}

	/**
	 * @return the roomNr
	 */
	public String getRoomNr() {
		return roomNr;
	}

	/**
	 * @param roomNr the roomNr to set
	 */
	public void setRoomNr(String roomNr) {
		this.roomNr = roomNr;
	}

	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the roomCapacity
	 */
	public Integer getRoomCapacity() {
		return roomCapacity;
	}

	/**
	 * @param roomCapacity the roomCapacity to set
	 */
	public void setRoomCapacity(Integer roomCapacity) {
		this.roomCapacity = roomCapacity;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return the roomBuilding
	 */
	public String getRoomBuilding() {
		return roomBuilding;
	}

	/**
	 * @param roomBuilding the roomBuilding to set
	 */
	public void setRoomBuilding(String roomBuilding) {
		this.roomBuilding = roomBuilding;
	}

}
