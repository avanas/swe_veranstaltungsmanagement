/**
 * 
 */
package DAOs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Events.Event;
import Rooms.Room;

/**
 * @author vmb
 *
 */


public abstract class DAO extends HttpServlet{
	
	/**
	 * 
	 */
	java.sql.Connection con=null;
	java.sql.Statement stmt=null;
	
	private static final long serialVersionUID = 1L;
	

    public DAO() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		
		try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				String database = "jdbc:oracle:thin:@oracle-lab.cs.univie.ac.at:1521:lab";
				String user = "a1168316";
				String pass = "unicore13";      
		      
		      	// establish connection to database
				con = DriverManager.getConnection(database, user, pass);
				stmt = con.createStatement();  
		
		} catch (Exception e) {
	
		      System.err.println(e.getMessage());
		      }
	}
	
	protected void closeConnection(java.sql.Connection con, java.sql.Statement stmt){
		
		try {
			stmt.close();
			con.close();
			
		} catch (Exception e) {
			
		      System.err.println(e.getMessage());
		      }
				
	}
				
		      	
		    
		
		
		

	/**
	 * 
	 */
	public void saveUser() {
	}
	
	/**
	 * 
	 */
	public void deleteUser() {
	}
	
	/**
	 * 
	 */
	public void editUser() {
	}
	
	/**
	 * @return
	 */
	public Event getEventsOfUser() {
		return null;
	}
	
	/**
	 * @return
	 */
	public ArrayList<Event> getEventList() {
		return null;
	}
	
	/**
	 * @param event
	 */
	public void saveEvent(Event event) {
	}
	
	/**
	 * @param event
	 */
	public void updateEvent(Event event) {
	}
	
	/**
	 * @param event
	 */
	public void deleteEvent(Event event) {
	}
	
	/**
	 * @param eventName
	 * @return
	 */
	public Event getEventbyName(String eventName) {
		return null;
	}
	
	/**
	 * @return
	 */
	public ArrayList<Room> getRoomList() {
		return null;
	}
	
	/**
	 * @param capacity
	 * @param type
	 */
	public void getRoom(Integer capacity, String type) {
	}
	
	/**
	 * @param eventName
	 * @param eventDate
	 * @param eventType
	 * @param eventDiscipline
	 * @param eventRoom
	 * @param eventTime
	 * @param eventDuration
	 */
	public void EventSearch(String eventName, Date eventDate, String eventType, String eventDiscipline, String eventRoom, Time eventTime, Integer eventDuration) {
	}

}
