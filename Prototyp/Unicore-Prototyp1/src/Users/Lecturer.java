package Users;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import Events.Event;
/**
 * 
 */

/**
 * @author vmb
 *
 */
public class Lecturer extends User{

	String discipline;
	String LecturerID;
	
	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param fristName
	 * @param lastName
	 * @param birthday
	 * @param discipline
	 * @param LecturerID
	 */
	public Lecturer(String username, String email, String password,
			String fristName, String lastName, Date birthday, String discipline, String LecturerID) {
		super(username, email, password, fristName, lastName, birthday);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the discipline
	 */
	public String getDiscipline() {
		return discipline;
	}

	/**
	 * @param discipline the discipline to set
	 */
	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	/**
	 * @return the lecturerID
	 */
	public String getLecturerID() {
		return LecturerID;
	}

	/**
	 * @param lecturerID the lecturerID to set
	 */
	public void setLecturerID(String lecturerID) {
		LecturerID = lecturerID;
	}
	
	/**
	 * @param eventType
	 * @param eventName
	 * @param eventCapacity
	 * @param eventDate
	 * @param eventRoomType
	 * @param eventDiscipline
	 * @param eventTime
	 * @param eventDuration
	 * @return
	 */
	public Event createEvent(String eventType, String eventName, Integer eventCapacity, Date eventDate, String eventRoomType, String eventDiscipline, Time eventTime, Integer eventDuration){
		return null;
		
	}
	
	/**
	 * @return
	 */
	public ArrayList<Event> getTimeTable(){
		return null;
		
	}
	
	/**
	 * @param eventName
	 * @param eventType
	 * @param eventDiscipline
	 */
	public void editEvent(String eventName, String eventType, String eventDiscipline){
		
	}
	
	/**
	 * @param event
	 */
	public void deleteEvent(Event event){
		
	}
}
