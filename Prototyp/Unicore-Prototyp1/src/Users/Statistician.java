package Users;

import java.util.Date;
/**
 * 
 */

/**
 * @author vmb
 *
 */
public class Statistician extends User{
	
	String statisticianID;

	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param fristName
	 * @param lastName
	 * @param birthday
	 * @param statistician
	 */
	public Statistician(String username, String email, String password,
			String fristName, String lastName, Date birthday, String statistician) {
		super(username, email, password, fristName, lastName, birthday);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the statisticianID
	 */
	public String getStatisticianID() {
		return statisticianID;
	}

	/**
	 * @param statisticianID the statisticianID to set
	 */
	public void setStatisticianID(String statisticianID) {
		this.statisticianID = statisticianID;
	}

	/**
	 * @param roomType
	 * @return
	 */
	public String evaluateLoadbyRoomType(String roomType){
		return null;
		
	}
	
	/**
	 * @param roomCapacity
	 * @return
	 */
	public Integer evaluateLoadbyCapacity(Integer roomCapacity){
		return null;
		
	}
	
	/**
	 * @param Discipline
	 * @return
	 */
	public Integer evaluateNrOfStudentsByDiscipline(String Discipline){
		return null;
		
	}
	
	/**
	 * @param Discipline
	 * @return
	 */
	public Integer evaluateDropOutbyDiscipline(String Discipline){
		return null;
		
	}
	
	/**
	 * @param Lecturer
	 * @return
	 */
	public String evaluateResultsByLecturer(String Lecturer){
		return null;
		
	}
}
