package Users;
import Events.Event;

import java.util.ArrayList;
import java.util.Date;
/**
 * 
 */

/**
 * @author vmb
 *
 */
public class Student extends User{
	
	String discipline;
	String MatrNr;

	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param fristName
	 * @param lastName
	 * @param birthday
	 * @param discipline
	 * @param MatrNr
	 */
	public Student(String username, String email, String password,
			String fristName, String lastName, Date birthday, String discipline, String MatrNr) {
		super(username, email, password, fristName, lastName, birthday);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the discipline
	 */
	public String getDiscipline() {
		return discipline;
	}

	/**
	 * @param discipline the discipline to set
	 */
	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	/**
	 * @return the matrNr
	 */
	public String getMatrNr() {
		return MatrNr;
	}

	/**
	 * @param matrNr the matrNr to set
	 */
	public void setMatrNr(String matrNr) {
		MatrNr = matrNr;
	}

	/**
	 * @param EventName
	 */
	public void subscribeEvent(String EventName){
		
	}
	
	/**
	 * @return
	 */
	public ArrayList<Event> getTimeTable(){
		return null;
		
	}
	
	/**
	 * @param event
	 */
	public void unsubscribeEvent(Event event){
		
	}
	
	/**
	 * @param events
	 * @return
	 */
	public boolean checkEventRegistration(Event events){
		return false;
		
	}
}
