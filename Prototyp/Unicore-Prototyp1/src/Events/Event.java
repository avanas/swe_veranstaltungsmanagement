/**
 * 
 */
package Events;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import Users.Student;

/**
 * @author vmb
 *
 */
public abstract class Event {
	
	/**
	 * 
	 */
	String eventName;
	Date eventDate;
	String eventType;
	String eventDiscipline;
	Integer eventCapacity;
	String eventRoom;
	ArrayList<Student> attendees;
	Time eventTime;
	Integer eventDuration;
	String eventLecturer;
	
	/**
	 * @param eventName
	 * @param eventDate
	 * @param eventType
	 * @param eventDiscipline
	 * @param eventCapacity
	 * @param eventRoom
	 * @param attendees
	 * @param eventTime
	 * @param eventDuration
	 * @param eventLecturer
	 */
	public Event(String eventName, Date eventDate, String eventType, String eventDiscipline, Integer eventCapacity, String eventRoom, ArrayList<Student> attendees, Time eventTime, Integer eventDuration, String eventLecturer){
		
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @return the eventDate
	 */
	public Date getEventDate() {
		return eventDate;
	}

	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return the eventDiscipline
	 */
	public String getEventDiscipline() {
		return eventDiscipline;
	}

	/**
	 * @param eventDiscipline the eventDiscipline to set
	 */
	public void setEventDiscipline(String eventDiscipline) {
		this.eventDiscipline = eventDiscipline;
	}

	/**
	 * @return the eventCapacity
	 */
	public Integer getEventCapacity() {
		return eventCapacity;
	}

	/**
	 * @param eventCapacity the eventCapacity to set
	 */
	public void setEventCapacity(Integer eventCapacity) {
		this.eventCapacity = eventCapacity;
	}

	/**
	 * @return the eventRoom
	 */
	public String getEventRoom() {
		return eventRoom;
	}

	/**
	 * @param eventRoom the eventRoom to set
	 */
	public void setEventRoom(String eventRoom) {
		this.eventRoom = eventRoom;
	}

	/**
	 * @return the attendees
	 */
	public ArrayList<Student> getAttendees() {
		return attendees;
	}

	/**
	 * @param attendees the attendees to set
	 */
	public void setAttendees(ArrayList<Student> attendees) {
		this.attendees = attendees;
	}

	/**
	 * @return the eventTime
	 */
	public Time getEventTime() {
		return eventTime;
	}

	/**
	 * @param eventTime the eventTime to set
	 */
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	/**
	 * @return the eventDuration
	 */
	public Integer getEventDuration() {
		return eventDuration;
	}

	/**
	 * @param eventDuration the eventDuration to set
	 */
	public void setEventDuration(Integer eventDuration) {
		this.eventDuration = eventDuration;
	}

	/**
	 * @return the eventLecturer
	 */
	public String getEventLecturer() {
		return eventLecturer;
	}

	/**
	 * @param eventLecturer the eventLecturer to set
	 */
	public void setEventLecturer(String eventLecturer) {
		this.eventLecturer = eventLecturer;
	}

}
