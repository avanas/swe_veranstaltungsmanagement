/**
 * 
 */
package Events;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import Users.Student;

/**
 * @author vmb
 *
 */
public class Lecture extends Event{
	
	/**
	 * 
	 */
	boolean examImmanent;
	boolean repeatedWeekly;

	/**
	 * @param eventName
	 * @param eventDate
	 * @param eventType
	 * @param eventDiscipline
	 * @param eventCapacity
	 * @param eventRoom
	 * @param attendees
	 * @param eventTime
	 * @param eventDuration
	 * @param eventLecturer
	 * @param examImmanent
	 * @param repeatedWeekly
	 */
	public Lecture(String eventName, Date eventDate, String eventType,
			String eventDiscipline, Integer eventCapacity, String eventRoom,
			ArrayList<Student> attendees, Time eventTime,
			Integer eventDuration, String eventLecturer, boolean examImmanent, boolean repeatedWeekly) {
		super(eventName, eventDate, eventType, eventDiscipline, eventCapacity,
				eventRoom, attendees, eventTime, eventDuration, eventLecturer);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the examImmanent
	 */
	public boolean isExamImmanent() {
		return examImmanent;
	}

	/**
	 * @param examImmanent the examImmanent to set
	 */
	public void setExamImmanent(boolean examImmanent) {
		this.examImmanent = examImmanent;
	}

	/**
	 * @return the repeatedWeekly
	 */
	public boolean isRepeatedWeekly() {
		return repeatedWeekly;
	}

	/**
	 * @param repeatedWeekly the repeatedWeekly to set
	 */
	public void setRepeatedWeekly(boolean repeatedWeekly) {
		this.repeatedWeekly = repeatedWeekly;
	}
	
	

}
