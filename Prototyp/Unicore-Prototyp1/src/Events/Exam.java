/**
 * 
 */
package Events;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import Users.Student;

/**
 * @author vmb
 *
 */
public class Exam extends Event{
	
	/**
	 * 
	 */
	Lecture ExamLecture;
	ArrayList<Student> AttendeeResult;

	/**
	 * @param eventName
	 * @param eventDate
	 * @param eventType
	 * @param eventDiscipline
	 * @param eventCapacity
	 * @param eventRoom
	 * @param attendees
	 * @param eventTime
	 * @param eventDuration
	 * @param eventLecturer
	 * @param ExamLecture
	 * @param AttendeeResult
	 */
	public Exam(String eventName, Date eventDate, String eventType,
			String eventDiscipline, Integer eventCapacity, String eventRoom,
			ArrayList<Student> attendees, Time eventTime,
			Integer eventDuration, String eventLecturer, Lecture ExamLecture, ArrayList<Student> AttendeeResult) {
		super(eventName, eventDate, eventType, eventDiscipline, eventCapacity,
				eventRoom, attendees, eventTime, eventDuration, eventLecturer);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the examLecture
	 */
	public Lecture getExamLecture() {
		return ExamLecture;
	}

	/**
	 * @param examLecture the examLecture to set
	 */
	public void setExamLecture(Lecture examLecture) {
		ExamLecture = examLecture;
	}

	/**
	 * @return the attendeeResult
	 */
	public ArrayList<Student> getAttendeeResult() {
		return AttendeeResult;
	}

	/**
	 * @param attendeeResult the attendeeResult to set
	 */
	public void setAttendeeResult(ArrayList<Student> attendeeResult) {
		AttendeeResult = attendeeResult;
	}
	
}
