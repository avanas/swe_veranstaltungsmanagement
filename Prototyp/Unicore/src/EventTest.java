import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import DAOs.*;
import Events.*;
import Management.*;

public class EventTest {

	EventManagement em;
	EventDAO ed;
	Event event;
	Event lecture;
	Event exam;
	
	@Before 
	public void setUp() throws Exception {
		em = new EventManagement();
		ed = new EventDAO();
	}
	
	//Test actual event classes
	@Test 
	public void testCreateEvent(){
		event = new Event("WS2013", "eventType", "eventName", "eventDiscipline", 100, "13:30", 90, "1234567", "01.10.2013", "31.01.2014");
	}
	
	@Test 
	public void testCreateLecture(){
		lecture = new Lecture("lecture", "WS2013", "lecture", "Biologie", 450, "09:15", 90, "0157284", 1, "03.10.2013", "30.11.2013");
	}	
	
	@Test 
	public void testCreateExam(){
		exam = new Exam("eventName", "SS2014", "07.06.2014", "Astronomie", 30, "09:00", 120, "0275937", "1");
	}
	
	//Test eventManagement and eventDAO
	
	@Test
	public void testFindRoom() throws Exception {
		System.out.println(em.checkForRoom("01.01.2014", "01.01.2014", "Hoersaal", 10, "10:30", 60));
	}
	
	
	@Test(expected=Exception.class)
	public void testNotFindRoom() throws Exception {
		em.checkForRoom("20.01.2014", "20.10.2014", "Hoersaal", 10000, "20:30", 60);

	}
	
	@Test
	public void testBookRoom() {
		em.bookRoom("00001", "001", "04.01.2013", "04.01.2013", "08:30", 60, "WS2013");
	}
	
	@Test
	public void testBookRoomFailure() {
		em.bookRoom("00001", "001", "02.10.2013", "29.01.2014", "19:00", 120, "WS2013");
	}
	
	@Test
	public void testGetEventsOfUser() {
		ed.getEventsOfUser("student1");
	}
	
	@Test
	public void addEventSuccess() {

		
	}
	
	@Test
	public void addEventFailure() {

	}
	
	@Test
	public void addResultsForLecture() {

	}
	
	@Test
	public void addResultsForExam() {

	}

	@Test
	public void addResultsForOtherEventFailure() {

	}

	@Test
	public void testDeleteGeneralEvent() {

	}

	@Test
	public void testDeleteLecture() {

	}
	
	@Test
	public void testDeleteExam() {

	}

	
	@Test
	public void editEventName() {

	}
	
	@Test
	public void editEventLecturer() {

	}
	
	
	@Test
	public void testGetAttendees() {

	}
	
	@Test
	public void testGetGeneralEvent() {
		Event e = new Event(null, null, null, null, null, null, null, null, null, null);
		e = em.getEvent("2");
		assertEquals("90", e.getEventDuration());
		assertEquals("Medienwissenschaft", e.getEventName());
		
	}
	
	@Test
	public void testGetLecture() {
		em.getEvent("1");
	}
	
	@Test
	public void testGetExam() {

	}
	
	@Test
	public void testGetResultOfGeneralEvent() {

	}
	
	@Test
	public void testGetResultOfLecture() {

	}
	
	@Test
	public void testGetResultOfExam() {

	}
	
	/*
	@Test
	public void searchByName() {
		em.searchEvent("Softwareengineering", null, "lecture", null, null, null, null, null, "WS2013");

	}
*/
	
	@Test
	public void searchByID() {

	}
	
	@Test
	public void searchByLecturerName() {

	}
	
	@After
	public void tearDown () throws Exception {
		em = null;
		ed = null;
	}
	
}
