/**
 * Defines the management classes of the Unicore system. 
 * The classes contained in this package provide the main functionality of the system.
 * They handle JSP-File requests and call the needed functions in the EventDAO and UserDAO in order to access the database.  
 */

package Management;
