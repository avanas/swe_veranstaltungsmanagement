/**
 * 
 */
package Management;

import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOs.*;
import Events.*;
import Users.*;
import Rooms.*;
import Management.UserManagement;




@WebServlet("/EventManagement")

/**
 * Is used to coordinate {@code Event} objects and tasks related to the {@code User} involved in these {@code Event} instances.
 * The class provides the functionality to store {@code Event} objects persistently in a database using the EventDAO.
 * The {@code EventManagement} can be accessed from a Servlet.
 *
 */
public class EventManagement extends HttpServlet{
	/**
	 * 
	 */
	EventDAO database;
	UserManagement usermgmt;
	
	
	public EventManagement(){
		this.database = new EventDAO();
		this.usermgmt = new UserManagement();
	}
	
	/**
	 * 
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. This variable is used to determine an element of which class to generate, i.e. if it says "lecture" a {@code Lecture} will be created, if it says "exam" an {@code Exam} will be created, otherwise an {@code Event} will be created.
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param roomType Specifies the type of {@code Room} an {@code Event} takes place.
	 * @param buildingID Specifies the building in which the {@code Event} takes place through an unique identifier.
	 * @param eventRoom Specifies the location of the {@code Event} through an unique identifier.
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param examImmanent Specifies whether presence during the event described in the {@code Lecture} is mandatory for participants. Value of 1 means the presence is mandatory, value of 0 means presence is not required.
	 * @param LVNr Identifier specific for {@code Lecture} objects.
	 * @param lectureType Describes the type of {@code Lecture}, e.g. "UE", "VO", "VU".
	 * @param eventFirstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param eventLastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. Note that for
	 * @param disciplineID Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param examLecture Specifies the Lecture the {@code Exam} belongs to through an unique identifier.
	 * @param dayOfWeek Specifies the day at which the {@code Event} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code eventFirstSession}
	 */
	
	public void addEvent(String eventID, String eventType, String eventName, String semester, Integer eventCapacity, String eventDiscipline, String roomType, String buildingID, String eventRoom, String eventTime, Integer eventDuration, Integer examImmanent, String LVNr, String lectureType, String first, String last, String disciplineID, String examLecture, String examDate, String dayOfWeek, String eventLecturer){
	// GET LECTURERID --> added input parameter eventLecturer
		
		if(eventType.equals("exam")){
			Exam exam = new Exam(eventName, semester, examDate, dayOfWeek, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examLecture, eventID);
			database.saveEvent(exam, eventRoom, buildingID, eventID);
			return;
		}
		if(eventType.equals("lecture")){
			Lecture lecture = new Lecture(eventName, semester, lectureType, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examImmanent, first, last, dayOfWeek, eventID, LVNr);
			System.out.println(lecture.getEventID());
			System.out.print("eventLecturer:");
			System.out.println(lecture.getEventLecturer());
			System.out.print("eventRoom:");
			System.out.println(eventRoom);
			database.saveEvent(lecture, eventRoom, buildingID, eventID);
			return;
		}
		Event event = new Event(semester, eventType, eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, first, last, dayOfWeek, eventID);
		database.saveEvent(event, eventRoom, buildingID, eventID);
	}
	
	/**
	 * @param eventID Unique identifier of {@code Event} objects
	 */
	public void deleteEvent(String eventID){
		database.deleteEvent(eventID);
	}
	
	/**
	 * Used to alter certain parameters of {@code Event} instances persistently stored in the database. Note that only parameters wished to alter should be given, others should be handed to the method as null. Method calls the EventDAO method updateEvent.
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param lecturerID Specifies the {@code Lecturer} associated with and responsible for the {@code Event} through an unique identifier.
	 * @param LVNr Identifier specific for {@code Lecture} objects.
	 * @param examImmanent Specifies whether presence during the event described in the {@code Lecture} is mandatory for participants. Value of 1 means the presence is mandatory, value of 0 means presence is not required.
	 */
	public void editEvent(String eventID, String eventName, String eventDiscipline, String lecturerID, String LVNr, Integer examImmanent){
		Event event = database.getEventbyID(eventID);
System.out.println(event.getEventName());
System.out.println(event.getEventLecturer());
		if(eventName != null) event.setEventName(eventName);
		if(lecturerID != null) event.setEventLecturer(lecturerID);
		if(eventDiscipline != null) event.setEventDiscipline(eventDiscipline);
		System.out.println("Made event");
		System.out.println(event.getEventLecturer());	
		System.out.println(event.getEventName());
		if(event instanceof Lecture){
			Lecture lecture = (Lecture) event;
				if(examImmanent != null) lecture.setExamImmanent(examImmanent);	
				database.updateEvent(lecture, eventID);
				return;
		}
System.out.println("OTHER EVENT");
		database.updateEvent(event, eventID);
	}
	
	/**
	 * Method is used to retrieve an {@code Event} from the database by its identifier.
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @return {@code Event} identifier.
	 */
	public Event getEvent(String eventID){
		Event event = database.getEventbyID(eventID);
		System.out.println("Return event in EventMgmt");
		return event;
	}
	
	/**
	 * Method accesses the database and attempts to retrieve the attendees registered for an event.
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @return List of attendees, objects of {@code User}, registered for the {@code Event}.
	 */	
	public ArrayList<Student> getAttendeesOfEvent(String eventID) {
		ArrayList<Student> attendees = new ArrayList<Student>();
		attendees = (ArrayList<Student>)(ArrayList<?>) database.getAttendees(eventID);
		return attendees;
		
	}
	
	/**
	 * Method searches a database for available rooms.
	 * @param firstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param lastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. Note that for
	 * @param eventRoomType Specifies the type of {@code Room} an {@code Event} takes place.
	 * @param capacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in, i.e. only rooms with enough space for at least this amount of participants will be returned.
	 * @param time Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param duration Specifies the duration of the event in minutes.
	 * @return List of available rooms identified by the criteria given.
	 * @throws Exception
	 */
	public ArrayList<Room> checkForRoom(String firstSession, String lastSession, String eventRoomType, Integer capacity, String time, Integer duration) throws Exception{
		// check if eventDate in past
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date firstEvent = new Date();
		Date lastEvent = new Date();
		try {
			
			firstEvent = dateFormat.parse(firstSession);
			lastEvent = dateFormat.parse(lastSession);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Calendar day = Calendar.getInstance();
		day.setTime(firstEvent);
		String dayOfWeek = day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
	
		
		Date current = new Date();
		if(current.after(firstEvent)){
			throw(new IllegalArgumentException("Cannot define Event taking place in the past. Select another date.\n"));
		}
		
		if(firstEvent.after(lastEvent)){
			throw(new IllegalArgumentException("Last session cannot take place before first session. \n"));
		}
		
		if(capacity == 0){
			throw(new IllegalArgumentException("Cannot create for zero participants.\n"));
		}
		
		// Time of event: No events beginning before 6 AM or ending after 11 PM
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		
		Date startTime = new Date();
		
		try {
			
			startTime = timeFormat.parse(time);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Calendar startOfEvent = Calendar.getInstance();
		Calendar endOfEvent = Calendar.getInstance();
		startOfEvent.setTime(startTime);
		endOfEvent.setTime(startTime);
		endOfEvent.add(Calendar.MINUTE,duration); 


		if(endOfEvent.get(Calendar.HOUR_OF_DAY) > 23 || endOfEvent.get(Calendar.HOUR_OF_DAY) == 23 && endOfEvent.get(Calendar.MINUTE) > 0){
			throw(new IllegalArgumentException("Cannot create event this late at night. Events may only last until 11PM.\n"));
		}
		
		if(startOfEvent.get(Calendar.HOUR_OF_DAY) < 6){
			throw(new IllegalArgumentException("Cannot create event this early in the morning. Events can only start from 6AM on.\n"));
		}
		
		
		ResultSet rooms = null;
		try {
			rooms = database.findRoomByCriteria(capacity, eventRoomType, firstSession, lastSession, dayOfWeek, time, duration);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(rooms == null) throw (new Exception ("No rooms matching the criteria.\n"));
		else {
			ArrayList<Room> roomList = database.parseRooms(rooms);
			return roomList;
		}
	}
	
	/**
	 * Method allows to reserve a {@code Room} at specific dates (Either one day or weekly repetition) and time for an {@code Event}. Method should only be called after {@code checkForRoom} and on rooms specified there as available.
	 * {@code Event} can only be created after a room has been reserved for it.  
	 * @param roomNr Identifier of a room, unique in a building
	 * @param buildingNr Unique identifier of buildings
	 * @param firstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param lastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. Note that for
	 * @param time Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param duration Specifies the duration of the event in minutes.
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @return unique identifier of the {@code Event} to be created.
	 */
	public String bookRoom(String roomNr, String buildingNr, String firstSession, String lastSession, String time, Integer duration, String semester){
		//Parse Date
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date dateOfEvent = new Date();
		try {
			
			dateOfEvent = dateFormat.parse(firstSession);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//Parse Beginning
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		
		Date startTime = new Date();
		
		try {
			
			startTime = timeFormat.parse(time);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//Parse End
		Calendar endOfEvent = Calendar.getInstance();
		endOfEvent.setTime(startTime);
		endOfEvent.add(Calendar.MINUTE,duration); 
		
		Calendar day = Calendar.getInstance();
		day.setTime(dateOfEvent);
		String dayOfWeek = day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
		
		
		return database.bookRoom(buildingNr, roomNr, firstSession, lastSession, dayOfWeek, time, duration, semester);
		
	}
	
	
	/**
	 * Method allows to add results to an event
	 * @param studentResults should contain in each row two columns, the first of which contains the identifier of the participant, the second the grade achieved by said participant
	 * @param eventID Unique identifier of the event for which results will be added. Needs to identify an exam or a lecture. If it is a lecture it needs to be examImmanent.
	 */
	public void addResults(Integer[][] studentResults, String eventID){
		System.out.println("eventID in EventManagement" + eventID);
		Event event = database.getEventbyID(eventID);
		if(event.getEventType().equals("exam")){
			database.setResults(studentResults, eventID);
		} else {
			if(event.getEventType().equals("lecture")) {
				Lecture lecture = (Lecture) event;
				if(lecture.isExamImmanent().equals(true)){
					database.setResults(studentResults, eventID);
				}
			} else {
				throw(new IllegalArgumentException("Cannot add results for this type of Event.\n"));
			}
		}
	}

	/**
	 * 
	 * @param eventID Unique identifier of an event
	 * @return list containing both identifiers of the the people that finished the class as well as their respective grades.
	 */
	public ArrayList<String> getResultsOfEvent(String eventID){
		return database.getResultsOfEvent(eventID);
	}
	
	//Alle retournierten User sind Studenten, kann aber aufgrund von Abhängigkeiten in anderen Funktionen nicht explizit ArrayList<Student> angeben.
	/**
	 * Method accessed the database and retrieves the registered Users of an event.
	 * @param eventID Unique identifier of the event we want to get information on
	 * @return List of {@code User} objects representing the people registered for the event
	 */	
	public ArrayList<User> getListOfAttendees(String eventID) {
		return database.getAttendees(eventID);
		
	}

	
	/**
	 * Performs a search for an event in a database based on the parameters given to the method. Note that for some search terms not only exact matches will be returned. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. This variable is used to determine an element of which class to generate, i.e. if it says "lecture" a {@code Lecture} will be created, if it says "exam" an {@code Exam} will be created, otherwise an {@code Event} will be created.
	 * @param disciplineID Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param disciplineName
	 * @param lecturerLastName
	 * @param lecturerFirstName
	 * @param lectureType Describes the type of {@code Lecture}, e.g. "UE", "VO", "VU".
	 * @param semester Required parameter.
	 * @return List of events fitting the search parameters.
	 */
	public ArrayList<Event> searchEvent(String eventName, String eventID, String eventType, String disciplineID, String disciplineName, String lecturerLastName, String lecturerFirstName, String lectureType, String semester) {

		if(semester == null) { 
			throw(new IllegalArgumentException("Semester must be defined.\n"));
		}
		
		return database.searchEvent(eventName, eventID, eventType, disciplineID, disciplineName, lecturerLastName, lecturerFirstName, lectureType, semester);
	}





	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		try {
			String formType = request.getParameter("formType");
			String currentUserType = (String) request.getSession().getAttribute("currentUserType");

			if (formType.equals("checkRoom")) {
				System.out.println("FormType equals checkRoom");
				if (currentUserType.equals("lecturer")) {
					HttpSession session = request.getSession(false);
					String firstSession = request.getParameter("firstsession");
					String lastSession = request.getParameter("lastsession");
					String eventRoomType = request.getParameter("type");
					Integer capacity = Integer.parseInt(request.getParameter("capacity"));
					String time = request.getParameter("eventtime");
					Integer duration = Integer.parseInt(request.getParameter("eventduration"));
					String semester = request.getParameter("semester");
					
					System.out.println(firstSession);
					System.out.println(lastSession);					
					System.out.println(eventRoomType);
					System.out.println(capacity);
					System.out.println(time);
					System.out.println(duration);
					System.out.println(semester);
					
					ArrayList<Room> freerooms = null;
					try {
						freerooms = (ArrayList<Room>) checkForRoom(firstSession, lastSession, eventRoomType, capacity, time, duration);
					} catch (Exception e) {
						e.printStackTrace();
					}
					//System.out.println(freerooms.get(0).getRoomName());
					//System.out.println(freerooms.get(1).getRoomBuildingName());
					session.setAttribute("rooms", freerooms);
					session.setAttribute("firstSession", firstSession);
					session.setAttribute("lastSession", lastSession);
					session.setAttribute("time", time);
					session.setAttribute("duration", duration);
					session.setAttribute("semester", semester);
					request.getRequestDispatcher("/chooseRoom.jsp").forward(request, response);
				}
				

			}
			
			if (formType.equals("EventDetails")) {
				System.out.println("FormType equals EventDetails");
				if (currentUserType.equals("lecturer")) {
					HttpSession session = request.getSession(false);
					String firstSession = (String) session.getAttribute("firstSession");
					String lastSession = (String) session.getAttribute("lastSession");
					String eventRoomType = (String) session.getAttribute("type");
					Integer capacity = (Integer) session.getAttribute("capacity");
					String time = (String) session.getAttribute("time");
					Integer duration = (Integer) session.getAttribute("duration");
					String semester = (String) session.getAttribute("semester");
					String eventID = (String) session.getAttribute("eventID");
					String roomNr = (String) session.getAttribute("roomNr");
					String buildingNr = (String) session.getAttribute("buildingNr");
					String eventName = request.getParameter("eventname");
					String eventtype = request.getParameter("eventtype");
					Integer eventcapacity = Integer.parseInt(request.getParameter("capacity"));
					String eventdiscipline = request.getParameter("disciplineID");
					Integer examImmanent = Integer.parseInt(request.getParameter("primmanent"));
					String lvnr = request.getParameter("lvnr");
					String examlecture = request.getParameter("examlecture");
					String lectureType = request.getParameter("lvtype");
					String lecturerID = (String) session.getAttribute("currentUserID");
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
					Date firstDate = new Date();
					try {
						firstDate = dateFormat.parse(firstSession);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Calendar day = Calendar.getInstance();
					day.setTime(firstDate);
					String dayOfWeek = day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
					
					
					System.out.println(firstSession);
					System.out.println(lastSession);					
					System.out.println(eventRoomType);
					System.out.println(capacity);
					System.out.println(time);
					System.out.println(duration);
					System.out.println(semester);
					System.out.println(eventID);
					System.out.println(eventName);
					System.out.println(eventtype);
					System.out.println(eventcapacity);
					System.out.println(eventdiscipline);
					System.out.println(examImmanent);
					System.out.println(lvnr);
					System.out.println(examlecture);
					System.out.println(lectureType);
					System.out.println(roomNr);
					System.out.println(buildingNr);
					System.out.println(dayOfWeek);
					
					this.addEvent(eventID, eventtype, eventName, semester, eventcapacity, eventdiscipline, eventRoomType, buildingNr, roomNr, time, duration, examImmanent, lvnr, lectureType, firstSession, lastSession, eventdiscipline, examlecture, firstSession, dayOfWeek, lecturerID);
					System.out.println("vor Redirect");
					response.sendRedirect("UserManagement?pagerequest=mycourses");
					//request.getRequestDispatcher("/UserManagement?pagerequest=mycourses").forward(request, response);
				}
				

			}
			
			
			if (formType.equals("eventsbydiscipline")){
				String disciplineID = request.getParameter("disciplineID");
				String semester = request.getParameter("semester");
				System.out.println(disciplineID);
				ArrayList<Event> eventsbydiscipline = searchEvent(null, null, null, disciplineID, null, null, null, null, semester);
				request.setAttribute("events", eventsbydiscipline);
				request.getRequestDispatcher("/allEvents.jsp").forward(request, response);
			}
			
			if (formType.equals("eventsbyparameters")){
				String eventName = request.getParameter("eventname");
				String eventID = request.getParameter("eventID");
				String eventType = request.getParameter("eventType");
				String disciplineID = request.getParameter("disciplineID");
				String disciplineName = request.getParameter("disciplineName");
				String lecturerFirstName = request.getParameter("lecturerFirstName");
				String lecturerLastName = request.getParameter("lecturerLastName");
				String lectureType = request.getParameter("lectureType");
				String semester = request.getParameter("semester");

				ArrayList<Event> eventsbyparameters = database.searchEvent(eventName, eventID, eventType, disciplineID, disciplineName, lecturerLastName, lecturerFirstName, lectureType, semester);
				request.setAttribute("events", eventsbyparameters);
				request.getRequestDispatcher("/allEvents.jsp").forward(request, response);
			}
			
			if (formType.equals("allEvents")){
				ArrayList<Event> allevents = database.getEventList();
				request.setAttribute("events", allevents);
				request.getRequestDispatcher("/allEvents.jsp").forward(request, response);
			}
			
			if (formType.equals("setResults")){
				System.out.println("handling of setResults called");
				String grade = request.getParameter("result");
				String eventID = request.getParameter("eventID");
				 
				String[] results = request.getParameterValues("result");
				String[] matrnr = request.getParameterValues("matrnr");
				Integer studentResults[][] = new Integer[results.length][2];
				
				if (grade != null){
					for (int i=0; i<results.length; i++){
						studentResults[i][0] = Integer.parseInt(matrnr[i]);
						studentResults[i][1] = Integer.parseInt(results[i]);
					}
				}
				
				if (grade != null){
					for (int i=0; i<studentResults.length; i++){
						System.out.println(studentResults[i][0] + " " + studentResults[i][1]);
					}
				}
				
				addResults(studentResults, eventID);
				System.out.println("sendRedirect_pagerequest=getAttendees&eventID="+eventID);
				response.sendRedirect("EventManagement?pagerequest=getAttendees&eventID="+eventID);
			}
			
			
			
			
		} catch (IOException e) {
		      System.out.println(e);
		      }
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		HttpSession session = request.getSession(false);
		String pagerequest = "";
		pagerequest = request.getParameter("pagerequest");
		System.out.println(pagerequest);
		String currentUserType = (String) request.getSession().getAttribute("currentUserType");
		String currentUserName = (String) request.getSession().getAttribute("currentUser");
		
		if (pagerequest.equals("bookroom")){
			if (!(request.getParameter("roomNr").isEmpty())) {
				String roomNr = request.getParameter("roomNr");
				String buildingNr = request.getParameter("buildingNr");
				String firstSession = (String) session.getAttribute("firstSession");
				String lastSession = (String) session.getAttribute("lastSession");
				String time = (String) session.getAttribute("time");
				Integer duration = (Integer) session.getAttribute("duration");
				String semester = (String) session.getAttribute("semester");
				
				System.out.println(roomNr);
				System.out.println(buildingNr);
				System.out.println(firstSession);
				System.out.println(lastSession);
				System.out.println(time);
				System.out.println(duration);
				System.out.println(semester);
				System.out.println("book room to go");
				
				String eventID = this.bookRoom(roomNr, buildingNr, firstSession, lastSession, time, duration, semester);
				System.out.println(eventID);
				session.setAttribute("eventID", eventID);
				session.setAttribute("buildingNr", buildingNr);
				session.setAttribute("roomNr", roomNr);				
				request.getRequestDispatcher("/EventInsertInfo.jsp").forward(request, response);

			} 
		}
		
		if (pagerequest.equals("eventdetail")) {
			String eventID = request.getParameter("eventID");
			Boolean eventreg = false;
			Event eventdetail = getEvent(eventID);
			System.out.println(currentUserName);
			System.out.println(currentUserType);
			System.out.println(eventreg);
			if (currentUserType == "student"){
				eventreg = usermgmt.checkEventRegistration(eventID, currentUserName);
			}
			if(eventreg) System.out.println("angemeldet");
			System.out.println(eventdetail.getEventName());
			request.setAttribute("eventreg", eventreg);
			System.out.println(eventreg);
			request.setAttribute("eventdetail", eventdetail);
			request.setAttribute("eventTypeVar", eventdetail.getEventType());
			if (eventdetail.getEventType().equals("lecture")){
				Lecture lecture = (Lecture) eventdetail;
				request.setAttribute("examImmanent", lecture.isExamImmanent());
			} else request.setAttribute("examImmanent", false);
			
			
			
			request.setAttribute("eventLecturer", eventdetail.getEventLecturer());
			request.getRequestDispatcher("/eventViewDetail.jsp").forward(request, response);

					
		}
		
		if (pagerequest.equals("deleteEvent")) {
			if (currentUserType.equals("lecturer")){
				String eventID = request.getParameter("eventID");
				deleteEvent(eventID);
				response.sendRedirect("UserManagement?pagerequest=mycourses");
			}
		}
		
		if (pagerequest.equals("getAttendees")) {
			if (currentUserType.equals("lecturer")){
				String eventID = request.getParameter("eventID");
				ArrayList <Student> attendeeList = getAttendeesOfEvent(eventID);
				Integer resultsOfStudents[] = new Integer[attendeeList.size()];
				System.out.println(attendeeList.size());
				for (int i=0; i<attendeeList.size(); i++){
					resultsOfStudents[i]=usermgmt.getResultsOfUserByEvent(attendeeList.get(i).getUsername(), eventID);
					System.out.println(resultsOfStudents[i]);
				}
				if(attendeeList.size()==0){
					Integer results2[] = new Integer[1];
					results2[0] = 1;
					request.setAttribute("results",results2);
				} else { request.setAttribute("results", resultsOfStudents);}
				request.setAttribute("eventID", eventID);
				request.setAttribute("attendees", attendeeList);
				System.out.println("Attributes set");
				request.getRequestDispatcher("viewAttendees.jsp").forward(request, response);
			}				
		}
		
		else {
			System.out.println("Error get request");
		}
		
	}	
		
	
}
