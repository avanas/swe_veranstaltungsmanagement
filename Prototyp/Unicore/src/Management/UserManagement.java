/**
 * 
 */
package Management;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Users.*;
import DAOs.*;
import Events.*;
import Rooms.*;


@WebServlet("/UserManagement")

/**
 * Is used to coordinate {@code User} objects and tasks related to the {@code Event} involved in these {@code User} instances.
 * The class provides the functionality to store {@code User} objects persistently in a database using the UserDAO.
 * The {@code EventManagement} can be accessed from a Servlet.
 */
public class UserManagement extends HttpServlet {
	
	UserDAO database;
	EventDAO eventdatabase;
	

	public UserManagement(){
		this.database= new UserDAO();
		this.eventdatabase = new EventDAO();
	}

	
	/**
	 * Method to check if {@code userName} and {@code password} of the user are correct, by calling to methods of UserDAO.
	 * @param userName identifies the user.
	 * @param password protects user information.
	 * @return true if the login details are correct and false if not.
	 */
	public boolean loginUser(String userName, String password){ //added by cv
		if((this.database.proofUsername(userName).equals(userName))) {
			System.out.println("proofUsername - check");
			if (this.database.proofPassword(userName, password).equals(password)) {
				return true;
			}
		} 
		return false;
	}
	
	

	/**
	 * Method to alter user information by calling a method of UserDAO.
	 * @param username identifies the user.
	 * @param firstName the first name of the user.
	 * @param lastName the last name of the user.
	 * @param email the email address of the user.
	 * @param birthday the birthday of the user.
	 */
	public void editUser(String username, String firstName, String lastName, String email, String birthday){
		this.database.editUser(username, firstName, lastName, email, birthday);
		
	}
	
	/**
	 * Method to get all events the user subscribed to by calling a method in the EventDAO.
	 * @param userName identifies the user.
	 * @return the list of all events.
	 */
	public ArrayList <Event> getEventsOfUser(String userName){
		return eventdatabase.getEventsOfUser(userName);
	}
	
	/**
	 * Method to set the results of an user for an event by calling an method of the EventDAO.
	 * @param studentResults holds all results.
	 * @param eventID identifies the event.
	 */
	public void SetResultsOfUser(Integer[][] studentResults, String eventID){
		
		eventdatabase.setResults(studentResults, eventID);
	}
	
	/**
	 * Method to register/save a user in the database by calling a method of the UserDAO.
	 * @param username identifies the user.
	 * @param email email address of the user.
	 * @param password to protect the user information.
	 * @param firstName first name of the user.
	 * @param lastName last name of the user.
	 * @param birthday birthday of the user.
	 * @param discipline of which the user is getting registered to.
	 * @param matrNr identifies a user of type student.
	 * @param lecturerID identifies a user of type lecturer.
	 * @param statisticianID identifies a user of type statistician.
	 * @param type defines what type the user is.
	 */
	public void RegisterUser(String username, String email, String password, String firstName, String lastName, Date birthday, ArrayList <String> discipline, Integer matrNr, String lecturerID, String statisticianID, String type){
		System.out.println("vor if in RegisterUser - usertype:");
		System.out.println(type);
		if(type.equals("student")){
			Student student = new Student(username, email, password, firstName, lastName, birthday, discipline, matrNr);
			User user = (User) student;
			System.out.println("new User student created");
			System.out.println(student.getMatrNr());
			this.database.saveUser(user);
		}
		
		if(type.equals("lecturer")){
			User user = new Lecturer(username, email, password, firstName, lastName, birthday, discipline, lecturerID);
			this.database.saveUser(user);
		}
		
		if(type.equals("statistician")){
			User user = new Statistician(username, email, password, firstName, lastName, birthday, statisticianID);
			this.database.saveUser(user);
		}
	}
	
	
	/**
	 * Method to unsubscribe a user from an event by calling a method of the UserDAO.
	 * @param userName identifies the user.
	 * @param eventID identifies the event.
	 */
	public void unsubscribeEvent(String userName, String eventID){
		database.unsubscribeEvent(userName, eventID);
	}
	
	
	/**
	 * Method to subscribe a user to an event by calling a method of the UserDAO.
	 * @param eventID identifies the event.
	 * @param userName identifies the user.
	 * @param semester defines when the event is held.
	 */
	public void subscribeEvent(String eventID, String userName, String semester){
		database.subscribeEvent(eventID, userName, semester);
	}
	
	/**
	 * Method to check if the user is subscribed to an event by calling a method of the UserDAO.
	 * @param eventID identifies the event.
	 * @param userName identifies the user.
	 * @return true if the user is subscribed and false if not.
	 */
	public boolean checkEventRegistration(String eventID, String userName){
		System.out.println("userName in UserManagement");
		System.out.println(userName);
		return database.checkEventRegistration(eventID, userName);
	}
	
	/**
	 * Method to parse a list of events by calling a method of the EventDAO.
	 * @param eventList list of events.
	 * @return a parsed list of events.
	 */
	public ArrayList<Event> parseEvents(ResultSet eventList) {
		return eventdatabase.parseEvents(eventList);
	}
	
	/**
	 * Method to get events of the user of a specific week day by calling a method of the UserDAO.
	 * @param userName identifies the user.
	 * @param weekday defines the day of the week.
	 * @return returns the events.
	 */
	public ArrayList<Event> getTimeTable(String userName, String weekday){
		return database.getTimeTable(userName, weekday);
	}
	
	
	/**
	 * Method to evaluate usage of a specific room type by calling a method of the UserDAO..
	 * @param roomType defines the room type 
	 * @return usage in numbers
	 */
	public StringBuffer evaluateLoadbyRoomType(String roomType){
		return database.evaluateLoadbyRoomType(roomType);
	}
	

	/**
	 * Method to evaluate usage of a specific capacity by calling a method of the UserDAO.
	 * @param roomCapacity defines the capacity.
	 * @return usage in numbers.
	 */
	public StringBuffer evaluateLoadbyCapacity(Integer roomCapacity){
		return database.evaluateLoadbyCapacity(roomCapacity);
	}
	

	/**
	 * Method to evaluate the number of students by discipline by calling a method of the UserDAO.
	 * @param DisciplineID defines the discipline.
	 * @return number of students.
	 */
	public StringBuffer evaluateNrOfStudentsByDiscipline(String DisciplineID){
		return this.database.evaluateNrOfStudentsByDiscipline(DisciplineID);
	}
	
	
	
	/**
	 * Method to evaluate the average results of events by lecturer by calling a method of the UserDAO.
	 * @param LecturerID identifies the lecturer.
	 * @return average results.
	 */
	public float evaluateResultsByLecturer(String LecturerID){
		return database.evaluateResultsByLecturer(LecturerID);	
	}
	
	/**
	 * Method to get the result of an event of the user by calling a method of the UserDAO.
	 * @param username identifies the user.
	 * @param eventID identifies the event.
	 * @return event result.
	 */
	public Integer getResultsOfUserByEvent(String username, String eventID){
		return database.getResultOfUserByEvent(username, eventID);		
	}
	
	/**
	 * Method to get all results of the user by calling a method of the UserDAO.
	 * @param userName identifies the user.
	 * @return all results of the user.
	 */
	public ArrayList<String> getResultsOfUser(String userName){
		System.out.println("getResultsOfUser in Usermgmt called");
		return database.getResultsOfUser(userName);
	}
	
	/**
	 * Method to get a list of lecturers by discipline by calling a method of the UserDAO.
	 * @param disciplineID identifies the discipline.
	 * @return a list of lecturers.
	 */
	public ArrayList<User> getLecturerList(String disciplineID){
		System.out.println("getLecturerList in UserMgmt called");
		return database.getLecturerList(disciplineID);
	}
	
	/**
	 * Method to parse {@code studentResults} of the type ResultSet by calling a method of the UserDAO.
	 * @param studentResult
	 * @return the parsed list of {@code studentResults}
	 */
	public ArrayList<String> parseResultSet(ResultSet studentResult) {
		return database.parseResultSet(studentResult);
	}

	
	/**
	 * Method to edit information of an event by calling a method of the EventDAO.
	 * @param eventID identifies the event.
	 */
	public void editEvent(String eventID){
		
		eventdatabase.updateEvent(eventdatabase.getEventbyID(eventID), eventID);
	}
	
	/**
	 * Method to delete an event by calling a method of the UserDAO.
	 * @param eventID identifies the event.
	 */
	public void deleteEvent(String eventID){
		
		eventdatabase.deleteEvent(eventID);
	}


	/**
	 * Method to handle POST requests.
	 * @param request HTTP request.
	 * @param response HTTP response.
	 * @throws ServletException Exception defined for a HTTServlet.
	 * @throws IOException Exception defined for I/O errors.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		try {
			String formType = request.getParameter("formType");
			String currentUserType = (String) request.getSession().getAttribute("currentUserType");
			System.out.println(formType);
			if (formType.equals("login")) {
				String f_username = request.getParameter("username");
				String f_password = request.getParameter("password");
				
				if (loginUser(f_username, f_password)) {
					HttpSession session = request.getSession(true);
					session.setAttribute("currentUser", f_username);
					String userID = database.getUserID(f_username);
					System.out.println(userID);
					session.setAttribute("currentUserID", userID);
					String usertype = database.getUserType(f_username);
					session.setAttribute("currentUserType", usertype);
					System.out.println(usertype);
					session.setAttribute("status", "loggedin");
					response.sendRedirect("loggedIn.jsp");
				}
				else {
					response.sendRedirect("invalidLogin.jsp");
				};
				
			}

				if (formType.equals("registeruser")) {
						String usertype = request.getParameter("userType");
						String username = request.getParameter("username");
						String password = request.getParameter("password");
						String firstname = request.getParameter("firstname");
						String lastname = request.getParameter("lastname");
						String email = request.getParameter("email");
						String birthdate = request.getParameter("birthdate");
						ArrayList<String> discipline = new ArrayList<String>();
						discipline.add(request.getParameter("disciplineID"));
						String id = request.getParameter("matrnr");	
						Integer matrNr = Integer.parseInt(request.getParameter("matrnr"));
						
						System.out.println(usertype);
						System.out.println(username);
						System.out.println(password);
						System.out.println(firstname);
						System.out.println(lastname);
						System.out.println(email);
						System.out.println(birthdate);
						System.out.println(id);
						System.out.println(matrNr);
						System.out.println(discipline);

						SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
						Date birthday=null;
						try {
							birthday = df.parse(birthdate);
						} catch (Exception e) {
							e.printStackTrace();
						}
						RegisterUser(username, email, password, firstname, lastname, birthday, discipline, matrNr, id, id, usertype);
						System.out.println("User created?");
						response.sendRedirect("login.jsp");
				}
					

				if (formType.equals("statsbydiscipline")) {
					if (currentUserType.equals("statistician")) {
						HttpSession session = request.getSession(false);
						String disciplineID = (String) request.getParameter("disciplineID");
						System.out.println(disciplineID);
						String nrofstudents = evaluateNrOfStudentsByDiscipline(disciplineID).toString();
						System.out.println(nrofstudents);
						session.setAttribute("nrofstudents", nrofstudents);
						response.sendRedirect("stat_discipline.jsp");
					}
				}
				
				if (formType.equals("statsbyroomtype")) {
					if (currentUserType.equals("statistician")) {
						HttpSession session = request.getSession(false);
						String roomtype = request.getParameter("roomtype");
						String loadbyroomtype = evaluateLoadbyRoomType(roomtype).toString();
						System.out.println(loadbyroomtype);
						session.setAttribute("loadbyroomtype", loadbyroomtype);
						response.sendRedirect("stat_load.jsp");
					}	
				}
				
				if (formType.equals("statsbycapacity")) {
					if (currentUserType.equals("statistician")) {
						HttpSession session = request.getSession(false);
						Integer capacity = Integer.parseInt(request.getParameter("capacity"));
						String loadbycapacity = evaluateLoadbyCapacity(capacity).toString();
						session.setAttribute("loadbycapacity", loadbycapacity);
						response.sendRedirect("stat_load.jsp");
					}
				}
				
				if (formType.equals("resultsbylecturer")) {
					if (currentUserType.equals("statistician")) {
						String disciplineID = request.getParameter("disciplineID");
						ArrayList<Lecturer> lecturerlist = (ArrayList<Lecturer>) (ArrayList<?>) this.getLecturerList(disciplineID);
						ArrayList<String> lecturerid = new ArrayList<String>();
						
						System.out.println(lecturerlist.size());
						for(int i=0; i<lecturerlist.size();i++){
							lecturerid.add(lecturerlist.get(i).getLecturerID());
						}
						
						ArrayList<String> lecturerresults = new ArrayList<String>();
						for(int i=0; i<lecturerid.size();i++){
							lecturerresults.add(String.valueOf(this.evaluateResultsByLecturer(lecturerid.get(i))));
						}	
						if(lecturerlist.isEmpty()){lecturerid.add("Kein Resultat");}
						request.setAttribute("lecturerlist", lecturerlist);
						request.setAttribute("resultslecturer", lecturerresults);
						request.getRequestDispatcher("/stat_results.jsp").forward(request, response);
					}
				}			
		
			
		} catch (IOException e) {
		      System.out.println(e);
		      }
		
		
	}
	
	
	/**
	 * Method to handle GET requests.
	 * @param request HTTP request.
	 * @param response HTTP response.
	 * @throws ServletException Exception defined for a HTTServlet.
	 * @throws IOException Exception defined for I/O errors.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
	
			try {
				String pagerequest = "";
				pagerequest = request.getParameter("pagerequest");

				
				if (pagerequest.equals("logout")){
					HttpSession session = request.getSession(false);
					if (session != null) {
						request.getSession().removeAttribute("status");
						request.getSession().invalidate();
						response.sendRedirect("index.jsp");

					}	

				} 

				
				if (pagerequest.equals("mycourses")) {
					HttpSession session = request.getSession(false);
					String username = (String) session.getAttribute("currentUser");
					System.out.println(username); //DEBUG

					ArrayList<Event> userevents = getEventsOfUser(username);
					request.setAttribute("userevents", userevents);
					request.getRequestDispatcher("/mycourses.jsp").forward(request, response);
					
						

				}

				if (pagerequest.equals("myexams")) {
					HttpSession session = request.getSession(false);

					String username = (String) session.getAttribute("currentUser");
					String currentUserType = (String) session.getAttribute("currentUserType");

					System.out.println(username); //DEBUG
					System.out.println(currentUserType); //DEBUG

					if (currentUserType.equals("student")){
						ArrayList<String> userexams = getResultsOfUser(username);
						request.setAttribute("userexams", userexams);
						request.getRequestDispatcher("/myexams.jsp").forward(request, response);
					}
					else response.sendRedirect("index.jsp");

				}
				
				
				if (pagerequest.equals("subscribe")) {
					HttpSession session = request.getSession(false);
					String currentUser = (String) session.getAttribute("currentUser");
					String eventID = (String) request.getParameter("eventID");
					String semester = (String) request.getParameter("semester");
					System.out.println(currentUser);
					System.out.println(eventID);
					this.subscribeEvent(eventID, currentUser, semester);
					String url = "EventManagement?pagerequest=eventdetail&eventID=";
					url += eventID;
					System.out.println(url);
					response.sendRedirect(url);
					System.out.println("subscribed?");
				}
				
				if (pagerequest.equals("unsubscribe")) {
					HttpSession session = request.getSession(false);
					String currentUser = (String) session.getAttribute("currentUser");
					String eventID = (String) request.getParameter("eventID");
					System.out.println(currentUser);
					System.out.println(eventID);
					this.unsubscribeEvent(currentUser, eventID);
					String url = "EventManagement?pagerequest=eventdetail&eventID=";
					url += eventID;
					System.out.println(url);
					response.sendRedirect(url);
					System.out.println("unsubscribed?");
				}
				
				if (pagerequest.equals("timetable")) {
					HttpSession session = request.getSession(false);
					String currentUser = (String) session.getAttribute("currentUser");
					ArrayList<Event> events_mon = this.getTimeTable(currentUser, "Mon");
					ArrayList<Event> events_tue = this.getTimeTable(currentUser, "Tue");
					ArrayList<Event> events_wed = this.getTimeTable(currentUser, "Wed");
					ArrayList<Event> events_thu = this.getTimeTable(currentUser, "Thu");
					ArrayList<Event> events_fri = this.getTimeTable(currentUser, "Fri");
					ArrayList<Event> events_sat = this.getTimeTable(currentUser, "Sat");
					request.setAttribute("events_mon", events_mon);
					request.setAttribute("events_tue", events_tue);
					request.setAttribute("events_wed", events_wed);
					request.setAttribute("events_thu", events_thu);
					request.setAttribute("events_fri", events_fri);
					request.setAttribute("events_sat", events_sat);
					request.getRequestDispatcher("/timetable.jsp").forward(request, response);
				}
				
				else {
					System.out.println("Error get request");
				};
		

			} catch (IOException e) {
			      System.out.println(e);
			      }
			
	}
	

}

