package Management;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import org.junit.AfterClass;
import org.junit.Test;

import Users.User;
import DAOs.UserDAO;

public class JUnitUser {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testUserManagement() {
		fail("Not yet implemented");
	}

	@Test
	public void testEditUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEventsOfUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetResultsOfUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testRegisterUser() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		Date datum=null;
		try {
			datum = df.parse("08.04.1992");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		uman.RegisterUser("student3", "stu@test.com", "pw_stu3", "Fritz", "Gruber", datum);
		assertEquals(udao.proofUsername("student3"),true);
	}

	@Test
	public void testUnsubscribeEvent() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		try {				
			udao.establishConnection();	
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		uman.unsubscribeEvent("student1", "1");
		
		String sql;
		sql = "SELECT userName FROM Attending WHERE Attending.eventID='"+"1"+"'";
		ResultSet checkeventID=null;
		String resultusername=null;
		
		try {
			checkeventID=udao.stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while(checkeventID.next()){
				resultusername=checkeventID.getString("student1");
                   
            if(!(resultusername.equals("student1"))){
            	resultusername=null;
            	udao.closeConnection();
            
            }     
        }
		} catch (SQLException e2) {
             // TODO Auto-generated catch block
             e2.printStackTrace();
		}
		udao.closeConnection();
		
		assertEquals(resultusername,null);
		
		
	}

	@Test
	public void testGetUserType() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		assertEquals(udao.getUserType("student1"), "Student");
	}

	@Test
	public void testSubscribeEvent() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		try {				
			udao.establishConnection();	
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		uman.subscribeEvent("2", "student1", "WS2013");
		
		String sql;
		sql = "SELECT username FROM Attending WHERE Attending.eventID='"+"2"+"'";
		ResultSet checkeventID=null;
		String resultusername=null;
		
		try {
			checkeventID=udao.stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while(checkeventID.next()){
				resultusername=checkeventID.getString("student1");
                   
            if(resultusername.equals("student1")){
            	udao.closeConnection();
            
            }     
        }
		} catch (SQLException e2) {
             // TODO Auto-generated catch block
             e2.printStackTrace();
		}
		udao.closeConnection();
		
		assertEquals(resultusername,"student1");
	}

	@Test
	public void testCheckEventRegistration() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();

		assertEquals(uman.checkEventRegistration("1", "student1"),true);
	
	}

	@Test
	public void testParseEvents() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

	@Test
	public void testGetTimeTable() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

	@Test
	public void testEvaluateLoadbyRoomType() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		assertEquals(uman.evaluateLoadbyRoomType("Hoersaal"),"00001,001,HS1,Hoersaal,300");
	}

	@Test
	public void testEvaluateLoadbyCapacity() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		assertEquals(uman.evaluateLoadbyCapacity(300),"00001,001,HS1,Hoersaal,300");
	}

	@Test
	public void testEvaluateNrOfStudentsByDiscipline() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		assertEquals(uman.evaluateNrOfStudentsByDiscipline("033521"),"1");
	}

	@Test
	public void testEvaluateDropOutbyDiscipline() {
		fail("Not yet implemented");
	}

	@Test
	public void testEvaluateResultsByLecturer() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		assertEquals(uman.evaluateResultsByLecturer("87654321"),"1");
	}

	@Test
	public void testGetResultsOfUser() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

	@Test
	public void testParseResultSet() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

	@Test
	public void testCreateEvent() {
		/*UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
		
		try {				
			udao.establishConnection();	
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		uman.subscribeEvent("2", "student1", "WS2013");
		
		String sql;
		sql = "SELECT username FROM Attending WHERE Attending.eventID='"+"2"+"'";
		ResultSet checkeventID=null;
		String resultusername=null;
		
		
		assertEquals(uman.createEvent(semester, eventType, eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, eventRoom),);
	*/}

	@Test
	public void testEditEvent() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

	@Test
	public void testDeleteEvent() {
		UserManagement uman= new UserManagement();
		UserDAO udao= new UserDAO();
	}

}
