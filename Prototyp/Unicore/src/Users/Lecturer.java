package Users;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

import DAOs.*;
import Events.*;
/**
 * 
 */
import Management.*;

/**
 * @author vmb
 *
 */
public class Lecturer extends User{

	ArrayList<String> discipline;
	String LecturerID;
	
	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param birthday
	 * @param discipline
	 * @param LecturerID
	 */
    public Lecturer(String username, String email, String password,
            String firstName, String lastName, Date birthday, ArrayList<String> discipline, String LecturerID){
     super(username, email, password, firstName, lastName, birthday);
    
     setDiscipline(discipline);
     setLecturerID(LecturerID);
}
	
	/**
	 * @return the discipline
	 */
	public ArrayList<String> getDiscipline() {
		return discipline;
	}

	/**
	 * @param discipline the discipline to set
	 */
	public void setDiscipline(ArrayList<String> discipline) {
		this.discipline = discipline;
	}

	/**
	 * @return the lecturerID
	 */
	public String getLecturerID() {
		return LecturerID;
	}

	/**
	 * @param lecturerID the lecturerID to set
	 */
	public void setLecturerID(String lecturerID) {
		LecturerID = lecturerID;
	}
	
}
