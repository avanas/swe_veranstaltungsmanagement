/**
 * Defines the user objects of the Unicore system. 
 * The classes contained in this package do only provide datastructures with setters and getters.
 * These classes are required for the function of the system and passing information through the layers of the software system.  
 */

package Users;
