package Users;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import DAOs.*;
/**
 * 
 */

/**
 * @author vmb
 *
 */
public class Statistician extends User{
	
	String statisticianID;

	
	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param birthday
	 * @param statisticianID
	 */
	public Statistician(String username, String email, String password,
            String firstName, String lastName, Date birthday, String statisticianID) {
     super(username, email, password, firstName, lastName, birthday);
    
     setStatisticianID(statisticianID);
}
	
	/**
	 * @return the statisticianID
	 */
	public String getStatisticianID() {
		return statisticianID;
	}

	/**
	 * @param statisticianID the statisticianID to set
	 */
	public void setStatisticianID(String statisticianID) {
		this.statisticianID = statisticianID;
	}

}