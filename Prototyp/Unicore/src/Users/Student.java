package Users;
import DAOs.*;
import Events.*;
import Management.EventManagement;
import Management.UserManagement;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
/**
 * 
 */





import javax.servlet.ServletException;

/**
 * @author vmb
 *
 */
public class Student extends User{
	
	ArrayList<String> discipline;
	Integer MatrNr;

	
	/**
	 * @param username
	 * @param email
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param birthday
	 * @param discipline
	 * @param MatrNr
	 * @param usermanagement 
	 * @param eventmanagement 
	 */
	public Student(String username, String email, String password,
            String firstName, String lastName, Date birthday, ArrayList<String> discipline, Integer MatrNr) {
     super(username, email, password, firstName, lastName, birthday);
    
     setMatrNr(MatrNr);
     setDiscipline(discipline);
}

	/**
	 * @return the discipline
	 */
	public ArrayList<String> getDiscipline() {
		return discipline;
	}

	/**
	 * @param discipline the discipline to set
	 */
	public void setDiscipline(ArrayList<String> discipline) {
		this.discipline = discipline;
	}

	/**
	 * @return the matrNr
	 */
	public Integer getMatrNr() {
		return MatrNr;
	}

	/**
	 * @param matrNr the matrNr to set
	 */
	public void setMatrNr(Integer matrNr) {
		MatrNr = matrNr;
	}
	
}
