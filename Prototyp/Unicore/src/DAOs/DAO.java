/**
 * 
 */
package DAOs;

import java.io.IOException;
import java.sql.DriverManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author vmb
 *
 */


public abstract class DAO extends HttpServlet{
	
	/**
	 * 
	 */
	java.sql.Connection con=null;
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public boolean establishConnection() throws ServletException, IOException {
		 
		
		
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.err.println("Did not find class\n");
					return false;
				}
				String database = "jdbc:oracle:thin:@oracle-lab.cs.univie.ac.at:1521:lab";
				String user = "a1168316";
				String pass = "unicore13";      
		try {
				// establish connection to database
				this.con = DriverManager.getConnection(database, user, pass);
		} catch (Exception e) {
		      System.err.println(new Exception("Could not instantiate Database connection\n"));
		      return false;
		}
		return true;
	}
	
	public void closeConnection(){
		
		try {

			con.close();
			
		} catch (Exception e) {
			
		      System.err.println(e.getMessage());
		}
				
	}

}