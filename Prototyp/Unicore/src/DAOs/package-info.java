/**
 * Defines the DAO classes of the Unicore system. 
 * The classes contained in this package provide the functionality to interact with a database.
 * 
 */

package DAOs;
