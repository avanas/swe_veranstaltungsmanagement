/**
 * 
 */
package DAOs;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;

import Events.Event;
import Events.Exam;
import Events.Lecture;
import Rooms.Room;
import Users.Lecturer;
import Users.Statistician;
import Users.Student;
import Users.User;
import Events.*;


/**
 * The class is used to manage {@code Event} objects in a database, access information from the database and get information on the involvement of users in the event. 
 */
public class EventDAO extends DAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method is used to access the complete list of events stored in the database.
	 * @return List of all {@code Event} objects.
	 */
	public ArrayList<Event> getEventList() {
		java.sql.Statement stmt=null;
		
		//establish connection
		try {
			
			if(!establishConnection()) return null;
			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//Get list of Events
		String sql = "SELECT * FROM Event";
		ResultSet events = null;
		
		try {
			
			events = stmt.executeQuery(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		
		ArrayList<Event> event = parseEvents(events);
		
		closeConnection();
		return event;
	}

	/**
	 * Method is used to store events in the database. Note that the method is only to be called after bookRoom and requires the {@code roomID} and {@code eventID} obtained from {@code bookRoom}.
	 * @param event Object containing all parameters to be stored in the database as an event.
	 * @param eventRoom ID of the room in which the event takes place.
	 * @param eventID Unique identifier of the event.
	 */
	public void saveEvent(Event event, String eventRoom, String eventRoomBuilding, String eventID){
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;
		java.sql.Statement stmt3=null;
		
		try {
			
			establishConnection();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			stmt3 = con.createStatement();
			
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String sql = "INSERT INTO Event VALUES('"+eventID+"','"+event.getSemester()+"','"+event.getEventType()+"','"+event.getEventName()+"',"+"TO_DATE('"+event.getEventFirstSession()+"','DD.MM.YYYY'),TO_DATE('"+event.getEventLastSession()+"','DD.MM.YYYY'),'"+event.getDayOfWeek()+"',TO_DATE('"+event.getEventTime()+"','HH24:MI'),'"+event.getEventDuration()+"','"+event.getEventLecturer()+"','"+event.getEventDiscipline()+"','"+event.getEventCapacity()+"','"+eventRoom+"','"+eventRoomBuilding+"')";
		try {
			System.out.println(sql);
			stmt1.executeUpdate(sql);

		} catch(Exception e) {
			System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Event\n");
		}
		
		if(event instanceof Lecture){

			Lecture lecture = (Lecture) event;
			String sql1 = "INSERT INTO Lecture VALUES ('"+lecture.getLVNR()+"','"+eventID+"','"+lecture.getSemester()+"','"+lecture.getExamImmanent()+"','"+lecture.getLectureType()+"', TO_DATE('"+lecture.getEventFirstSession()+"','DD.MM.YYYY'), TO_DATE('"+lecture.getEventLastSession()+"','DD.MM.YYYY'),'"+event.getDayOfWeek()+"')";
			System.out.println(sql1);
			try {
				
				stmt2.executeUpdate(sql1);
	
			} catch(Exception e) {
				System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Lecture\n");
			}
		}
			
		if(event instanceof Exam){

			Exam exam = (Exam) event;
			try {
	
				stmt3.executeUpdate("INSERT INTO Exam (eventID, semester, LVNr, examDate) VALUES ('"+eventID+"','"+exam.getEventDiscipline()+"','"+exam.getSemester()+"',TO_DATE('"+exam.getEventLastSession()+"','DD.MM.YYYY'))");

			} catch(Exception e) {
			System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Exam\n");
			}
		}
		
		try {
			
			stmt1.close();
			stmt2.close();
			stmt3.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		
	}

	/**
	 * Method is used to alter events stored in the database. Method is called from the {@code EventManagement} method editEvent.
	 * @param event objects containing the current information. Note that this object will be altered in this method and the changed content will be handed to a database for persistent storage
	 * @param eventID Unique identifier of the event
	 */
	public void updateEvent(Event event, String eventID) {
		
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;
		System.out.println("Call");
		try {
			
			establishConnection();
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql, sql1, sql2;
		if(event instanceof Exam) {
			Exam exam = (Exam) event;
			sql = "UPDATE Exam SET semester='"+exam.getSemester()+"', LVNr='"+exam.getExamLecture()+"',examDate=TO_DATE('"+exam.getEventLastSession()+"','DD.MM.YYYY')";

			try {
				
				stmt.executeUpdate(sql);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if(event instanceof Lecture) {
			Lecture lecture = (Lecture) event;
			sql1 = "UPDATE Lecture SET semester='"+lecture.getSemester()+"', examimmanent='"+lecture.isExamImmanent()+"', lectureType='"+lecture.getLectureType()+"' WHERE eventID='"+eventID+"' ";

			try {
				
				stmt1.executeUpdate(sql1);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		sql2 = "UPDATE Event SET semester='"+event.getSemester()+"',name='"+event.getEventName()+"', lecturerID='"+event.getEventLecturer()+"', disciplineID='"+event.getEventDiscipline()+"' WHERE eventID='"+eventID+"' ";
		try {
			
			stmt2.executeUpdate(sql2);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			

		try {
		System.out.println("before close");
			stmt.close();
			stmt1.close();
			stmt2.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
	}

	/**
	 * Method calls cancelRoom to cancel the reservation of a room made for the {@code Event} after removing the stored event. 
	 * @param eventID Unique identifier identifying the event to be deleted.
	 */
	public void deleteEvent(String eventID) {
		
		
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;
		java.sql.Statement stmt3=null;
		java.sql.Statement stmt4=null;
		java.sql.Statement stmt5=null;
		java.sql.Statement stmt6=null;

		try {
			
			establishConnection();
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			stmt3 = con.createStatement();
			stmt4 = con.createStatement();
			stmt5 = con.createStatement();
			stmt6 = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
		try {
		//Look for event
		String sql, sql1, sql2, sql3, sql4, sql5, sql6;
		ResultSet resultEvent;
		sql = "SELECT * FROM Event WHERE eventID='"+eventID+"'";
		System.out.println(sql);
		resultEvent = stmt.executeQuery(sql);		
		resultEvent.next();
		
		//Delete Attendents
		sql1 = "DELETE Attending WHERE eventID='"+eventID+"'";
		System.out.println(sql1);
		stmt1.executeUpdate(sql1);	

		
		//First: Resolve dependencies
		//Check for exam
		if(resultEvent.getNString(3).equals("exam")){
			sql2 = "DELETE Exam WHERE eventID='"+eventID+"'";
			System.out.println(sql2);
			stmt2.executeUpdate(sql2);
		}
		
		//Check for lecture
		if(resultEvent.getNString(3).equals("lecture")){
			sql3 = "SELECT LVNr, semester FROM Lecture WHERE eventID='"+eventID+"'";
			System.out.println(sql3);
			ResultSet lecture = stmt.executeQuery(sql3);
			lecture.next();
			String LVNr = lecture.getNString(1);
			String semester = lecture.getNString(2);
			sql4 = "DELETE Exam WHERE LVNr='"+LVNr+"' AND semester='"+semester+"'";
			System.out.println(sql4);
			sql5 = "DELETE Lecture WHERE eventID='"+eventID+"'";
			stmt4.executeUpdate(sql4);
			System.out.println(sql5);
			stmt5.executeUpdate(sql5);
		}
		
		sql6 = "DELETE Event WHERE eventID='"+eventID+"'";
		System.out.println(sql6);
		stmt6.executeUpdate(sql6);
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		cancelRoom(eventID);

		
		try {
			
			stmt3.close();
			stmt4.close();
			stmt5.close();
			stmt6.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
	}

	/**
	 * Method looks up uniquely identified events in the database.
	 * @param eventID Unique identifier of an event
	 * @return event identified by the eventID
	 */
	public Event getEventbyID(String eventID) {
		java.sql.Statement stmt=null;

		try {
			
			if(!establishConnection()) return null;
			stmt = con.createStatement();

		} catch (ServletException e) {
			System.err.println("Servlet");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO");
			e.printStackTrace();
		} catch (SQLException e) {
			System.err.println("SQL");
			e.printStackTrace();
		}
		
		//get event from database
		ResultSet resultEvent = null;
		
		try {
			String sql = "SELECT * FROM Event WHERE eventID='"+eventID+"'";
			resultEvent = stmt.executeQuery(sql);
			System.out.println(sql);

		//	resultEvent.next();
		//	System.out.println(resultEvent.getString(1));
		} catch (SQLException e) {
			System.err.println("Error while trying to find event");
		}
		
	//	try {
			//if(resultEvent.next() == false) System.out.println("NOTHING");
			//resultEvent.next();
			//System.out.println(resultEvent.getString(1));
	/*	} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		Event event = parseEvent(resultEvent);

		try {
			stmt.close();
			closeConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return event;
	}
	
	/**
	 * Method looks up persistently stored events in the database based on the registration of a user.
	 * @param username Identifier of the user
	 * @return Returns list of all events a user is registered for.
	 */
	public ArrayList<Event> getEventsOfUser(String username) {
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;

		try {
			
			if(!establishConnection()) return null;
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ResultSet eventList = null;
		ResultSet eventListLecturer = null;

		
		//Identify all events
		
		String sql1 = "SELECT eventID, semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity FROM Event NATURAL JOIN Attending WHERE userName = '"+username+"' ";
		System.out.println(sql1);
		try {
			eventList = stmt.executeQuery(sql1);
			
		} catch (SQLException e) {
			System.out.println(e);
			return null;
			//e.printStackTrace();
		}
		
		
		ArrayList<Event> userEvents = parseEvents(eventList);
		
		
		Boolean firstResult = null;
		firstResult = userEvents.isEmpty();
		
		
		try {
			
			stmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (firstResult){
			System.out.println("eventList is null");
			String sql2 = "SELECT eventID, semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity FROM Event NATURAL JOIN Lecturer WHERE username='"+username+"'";
			System.out.println(sql2);
			try {
				eventListLecturer = stmt1.executeQuery(sql2);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			ArrayList<Event> userEventsLecturer = parseEvents(eventListLecturer);

			try {
				stmt1.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			closeConnection();
			return userEventsLecturer;
		}
		

		closeConnection();

		return userEvents;
		
	}

	/**
	 * 	Called before new {@code Event} can be created and passed to persistent storage.
	 * @param capacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the {@link Room} the event takes place in.
	 * @param roomType
	 * @param firstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param lastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param dayOfWeek Specifies the day at which the {@code Event} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code eventFirstSession}
	 * @param startTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param duration Specifies the duration of the event in minutes.
	 * @return Returns list of all free rooms that fit the criteria
	 * @throws Exception
	 */
	public ResultSet findRoomByCriteria(Integer capacity, String roomType, String firstSession, String lastSession, String dayOfWeek, String startTime, Integer duration) throws Exception {
		java.sql.Statement stmt=null;

		try {
			
			if(!establishConnection()) return null;
			stmt = con.createStatement();

		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ResultSet rooms = null;
		
		float dur = (float) duration;
		try {
			String sql = "(SELECT Room.roomID, Room.name, capacity, typ, Building.name, plz, street, Building.buildingID FROM Building JOIN Room ON Building.buildingID = Room.buildingID WHERE capacity >= "+capacity+" AND typ='"+roomType+"' AND NOT EXISTS (SELECT roomID, buildingID FROM takesPlace WHERE takesPlace.buildingID=Room.buildingID AND takesPlace.roomID=Room.roomID)) UNION (SELECT DISTINCT Room.roomID, Room.name, capacity, typ, Building.name, plz, street, building.buildingID FROM Building JOIN Room ON Building.buildingID = Room.buildingID JOIN takesPlace ON Building.buildingID = takesPlace.buildingID AND Room.roomID = takesPlace.roomID WHERE capacity >="+capacity+" AND typ='"+roomType+"' AND NOT EXISTS(SELECT roomID, buildingID FROM takesPlace WHERE dayOfWeek = '"+dayOfWeek+"' AND (lastSession > TO_DATE('"+firstSession+"','DD.MM.YYYY') OR firstSession < TO_DATE('"+lastSession+"','DD.MM.YYYY')) AND ((startTime+duration/1440)>TO_DATE('"+startTime+"','HH24:MI') OR (TO_DATE('"+startTime+"','HH24:MI')+"+dur/1440+")>startTime)))";

			System.out.println(sql);
			rooms = stmt.executeQuery(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(rooms == null) throw (new Exception ("No rooms matching the criteria.\n"));
		else return rooms;
	}
		
	/**
	 * Method makes a reservation of a room for an event. Reservations are defined by the room and the date and time. Note that if {@code eventFirstDate} is not equal to {@code eventLastDate} weekly repetition of the event is assumed and the room is booked for the given times in all weeks between those dates. 
	 * @param buildingID Specifies the building in which the {@code Event} takes place through an unique identifier.
	 * @param roomID
	 * @param eventFirstDate Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param eventLastDate Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param dayOfWeek Specifies the day at which the {@code Event} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code eventFirstSession}
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param duration Specifies the duration of the event in minutes.
	 * @param semester  Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @return eventID identifying the event for which the room has been reserved.
	 */
	public String bookRoom(String buildingID, String roomID, String eventFirstDate, String eventLastDate, String dayOfWeek, String eventTime, Integer duration, String semester){
		java.sql.Statement stmt=null;
	
		try {
			
			if(!establishConnection()) return null;

			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String eventID = null;
		
		try {
			// GET EVENT ID TO BE CREATED FROM DATABASE
//			ResultSet rs = stmt.executeQuery("SELECT seq_eventID.nextval from dual");
//			rs.next();
//			eventID = rs.getString(1);
//			System.out.println("eventID: "+eventID);
			
			String sql = "INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES ('"+roomID+"','"+buildingID+"','"+semester+"',TO_DATE('"+eventFirstDate+"','DD.MM.YYYY'),TO_DATE('"+eventLastDate+"','DD.MM.YYYY'),'"+dayOfWeek+"', TO_DATE('"+eventTime+"','HH24:MI'),'"+duration+"')";
			//rs.close();
			System.out.println(sql);
			stmt.executeUpdate(sql);
			
			String sql2 ="SELECT eventID FROM takesplace WHERE roomID='"+roomID+"' AND buildingID='"+buildingID+"' AND semester='"+semester+"' AND firstSession=TO_DATE('"+eventFirstDate+"','DD.MM.YYYY') AND lastSession=TO_DATE('"+eventLastDate+"','DD.MM.YYYY') AND dayOfWeek='"+dayOfWeek+"' AND startTime=TO_DATE('"+eventTime+"','HH24:MI') AND duration='"+duration+"'";
			ResultSet eventIDResult = stmt.executeQuery(sql2);
			eventIDResult.next();
			eventID = eventIDResult.getString(1);
			//String CHECK = "SELECT * FROM takesPlace";
			//ResultSet check = stmt.executeQuery(CHECK);
			//while(check.next()) { System.out.println(check.getString(1)+", "+check.getString(2)+", "+check.getString(3)); }
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			
			stmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		return eventID;
	}

	/**
	 * Method cancels the reservation of rooms made for an event as said event is deleted.
	 * @param eventID Unique identifier of the event which is in the process of being deleted.
	 */
	public void cancelRoom(String eventID){
		java.sql.Statement stmt=null;
		System.out.println("cancel Room");

		try {
			
			establishConnection();
			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql = "DELETE FROM takesPlace WHERE eventID='"+eventID+"'";
		System.out.println(sql);
		
		try {
			
			stmt.executeUpdate(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			
			stmt.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
	}
	
	/**
	 * Method allows for search of events by attributes of the event. Note that the parameter semester is required. All other parameters need not to be given. 
	 * @param eventName Name of the event. Must not be longer than 30 characters.
	 * @param eventID Unique identifier of {@code Event} objects.
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes {@link Lecture} and {@link Exam}. 
	 * @param disciplineID Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param disciplineName Name of the discipline an {@code Event} belongs to
	 * @param lecturerLastName Last name of the lecturer responsible for an {@code Event}
	 * @param lecturerFirstName First name of the lecturer responsible for an {@code Event}
	 * @param lectureType Describes the type of {@code Lecture}, e.g. "UE", "VO", "VU".
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @return List of {@code Event} objects fitting the criteria of the search
	 */
	public ArrayList<Event> searchEvent(String eventName, String eventID, String eventType, String disciplineID, String disciplineName, String lecturerLastName, String lecturerFirstName, String lectureType, String semester) {

		ResultSet events = null;
		java.sql.Statement stmt=null;
		
		try {
			
			if(!establishConnection()) return null;
			stmt = con.createStatement();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Determine sql-statement based on input variables
		StringBuffer sql = new StringBuffer();

		if(lectureType != null) {
			sql.append("SELECT Event.eventID, Event.semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity, Lecture.lectureType FROM (Event JOIN Discipline ON Event.disciplineID = Discipline.disciplineID)  JOIN Lecture ON Event.eventID=Lecture.eventID NATURAL JOIN Lecturer JOIN Person ON Person.userName = Lecturer.userName ");
		} else {
			sql.append("SELECT Event.eventID, Event.semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity FROM (Event JOIN Discipline ON Event.disciplineID = Discipline.disciplineID)  NATURAL JOIN Lecturer JOIN Person ON Person.userName = Lecturer.userName ");
		}

		sql.append("WHERE semester = '"+semester+"' ");
		
		if(eventType != null && !eventType.equals("")){
			sql.append("AND typ = '"+eventType+"' ");
		}
		if(eventName != null && !eventName.equals("") ){
			sql.append("AND Event.name LIKE '%"+eventName+"%' ");
		}
		if(eventID != null && !eventID.equals("")){
			sql.append("AND eventID = '"+eventID+"' ");
		}
		if(disciplineID != null && !disciplineID.equals("")){
			sql.append("AND Event.disciplineID = '"+disciplineID+"' ");
		}
		if(disciplineName != null && !disciplineName.equals("")){
			sql.append("AND discipline.name = '"+disciplineName+"' ");
		}
		if(lecturerLastName != null && !lecturerLastName.equals("")){
			sql.append("AND lastName = '"+lecturerLastName+"' ");
		}
		if(lecturerFirstName != null && !lecturerFirstName.equals("")){
			sql.append("AND firstName = '"+lecturerFirstName+"' ");
		}
		if(lectureType != null && !lectureType.equals("")){
			sql.append("AND lecture.lectureType = '"+lectureType+"' ");
		}
		
		try {
			events = stmt.executeQuery(sql.toString());
			//stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//closeConnection();
		ArrayList<Event> eventList = parseEvents(events);
		
		try {
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		closeConnection();
		
		return eventList;
	}
	
	/**
	 * Method retrieves the attendants registered for an event from the database.
	 * @param eventID Unique identifier of the event.
	 * @return List of attendants of an event
	 */
	public ArrayList<User> getAttendees(String eventID){
		ResultSet attendees = null;
		java.sql.Statement stmt=null;
		try {
			
			if(!establishConnection()) return null;
			
			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String sql;
		
		sql = "SELECT DISTINCT username, password, firstName, lastName, email, birthdate FROM Attending NATURAL JOIN Student NATURAL JOIN Person NATURAL JOIN Studies WHERE eventID = '"+eventID+"'";
		
		System.out.println(sql); //DEBUG
		
		try {

			attendees = stmt.executeQuery(sql);

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	//	closeConnection();
		ArrayList<User> userList = parseUsers(attendees);
		try {
			stmt.close();
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return userList;
	}

	/**
	 * Method translates the results of an SQL database query into an event.
	 * @param event Resulting single event obtained from an SQL query.
	 * @return {@code Event} object
	 */
	public Event parseEvent(ResultSet event) {
		System.out.println("parse 1");
		java.sql.Statement stmtParse=null;
		java.sql.Statement stmtParse1=null;
		ResultSet resultLecture=null;
		//establish connection
		try {
			
			//establishConnection();

			stmtParse = con.createStatement();
			stmtParse1 = con.createStatement();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		//Identify event type, add relevant information for each event type
		try {
			event.next();
			//Parse general Event
			String eventID = event.getString(1);
			String semester = event.getString(2);
			String eventType = event.getString(3);
			String eventName = event.getString(4);
			String firstSession = event.getString(5);
			String lastSession = event.getString(6);
			String dayOfWeek = event.getString(7);
			String eventTime = event.getString(8);
			Integer eventDuration = event.getInt(9);
			String eventLecturer = event.getString(10);
			String eventDiscipline = event.getString(11);
			Integer eventCapacity = event.getInt(12);
			
			System.out.println(eventID);
			
			if(eventType.equals("lecture")){
				//get lecture
				String sql2 = "SELECT lectureType, examimmanent, lvnr FROM Lecture WHERE eventID = '"+eventID+"'"; //"' AND semester = '"+semester+"' ";
				resultLecture = stmtParse1.executeQuery(sql2);
				
				
				//parse lecture
				resultLecture.next();
				Integer examImmanent = resultLecture.getInt(2);
				String lectureType = resultLecture.getNString(1);	
				String lvnr = resultLecture.getNString(3);

				resultLecture.close();
				//create lecture Object
				Lecture userEvent = new Lecture(eventName, semester, lectureType, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examImmanent, firstSession, lastSession, dayOfWeek, eventID, lvnr);

				stmtParse.close();
				stmtParse1.close();

				closeConnection();
				System.out.println("return userEvent");
				return userEvent;
				
			} else {
				if(eventType.equals("exam")) {
					//get exam
					String sql3 = "SELECT * FROM Exam WHERE eventID='"+eventID+"' ";
					ResultSet resultExam = stmtParse1.executeQuery(sql3);
					
					//parseExam
					resultExam.next();
					String examLecture = resultExam.getNString(2);
					String examDate = resultExam.getNString(3);
					
					//create exam Object
					Exam userEvent = new Exam(eventName, semester, examDate, dayOfWeek, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examLecture, eventID);

					stmtParse.close();
					stmtParse1.close();
					
					closeConnection();
					return userEvent;
					
				} else {
					//create event Object
					Event userEvent = new Event(semester, eventType, eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, firstSession, lastSession, dayOfWeek, eventID);

					stmtParse.close();
					stmtParse1.close();
					
					closeConnection();
					return userEvent;
					
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		return null;
	}
	
	/**
	 * Method translates the results of an SQL database query into a list of events
	 * @param eventList Resulting list of events obtained from an SQL query.
	 * @return List of {@code Event} objects.
	 */
	public ArrayList<Event> parseEvents(ResultSet eventList) {
		java.sql.Statement stmtParse=null;
		System.out.println("parseEvents called"); //DEBUG
		
		//establish connection
		try {
			
			establishConnection();

			stmtParse = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ArrayList<Event> userEvents = new ArrayList<Event>();
		
		//Identify event type, add relevant information for each event type
		try {
			while(eventList.next()){
				//Parse general Event
				String eventID = eventList.getNString(1);
				String semester = eventList.getNString(2);
				String eventType = eventList.getNString(3);
				String eventName = eventList.getNString(4);
				String firstSession = eventList.getNString(5);
				String lastSession = eventList.getNString(6);
				String dayOfWeek = eventList.getString(7);
				String eventTime = eventList.getNString(8);
				Integer eventDuration = eventList.getInt(9);
				String eventLecturer = eventList.getNString(10);
				String eventDiscipline = eventList.getNString(11);
				Integer eventCapacity = eventList.getInt(12);
				
				
				if(eventType.equals("lecture")){
					//get lecture
					String sql2 = "SELECT * FROM Lecture WHERE eventID = '"+eventID+"' AND semester = '"+semester+"'";
					ResultSet resultLecture = stmtParse.executeQuery(sql2);
					
					//parse lecture
					// String LVNr = resultLecture.getNString(1);
					while(resultLecture.next()){
						Integer examImmanent = resultLecture.getInt(4);
						String lectureType = resultLecture.getString(5);
						String lvnr = resultLecture.getString(1);
					
					//create lecture Object
					Lecture lecture = new Lecture(eventName, semester, lectureType, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examImmanent, firstSession, lastSession, dayOfWeek, eventID, lvnr);
					userEvents.add(lecture);
				}
				} else {
					if(eventType.equals("exam")) {
						//get exam
						String sql3 = "SELECT * FROM Exam WHERE eventID='"+eventID+"'";
						ResultSet resultExam = stmtParse.executeQuery(sql3);
						
						while(resultExam.next()){
						//parseExam
						String examLecture = resultExam.getNString(2);
						
						//create exam Object
						Exam exam = new Exam(eventName, semester, lastSession, dayOfWeek, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examLecture, eventID);
						userEvents.add(exam);
						}
					} else {
						//create event Object
						Event event = new Event(semester, eventType, eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, firstSession , lastSession, dayOfWeek, eventID);
						userEvents.add(event);
					}
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		try {
			stmtParse.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		return userEvents;
	}
	
	/**
	 * Method translates the results of an SQL database query into a list of rooms
	 * @param roomList Resulting list of rooms obtained from an SQL query.
	 * @return List of {@code Room} objects.
	 */
	public ArrayList<Room> parseRooms(ResultSet roomList) {
		System.out.println("parsRooms in EventDAO called");
		ArrayList<Room> rooms = new ArrayList<Room>();
		
		try {
			while(roomList.next()){
				
				//parse Room
				String roomNr = roomList.getString(1); 
				String roomName = roomList.getString(2);
				Integer capacity = roomList.getInt(3);
				String roomType = roomList.getString(4);
				String buildingName = roomList.getString(5);
				String plz = roomList.getString(6);
				String street = roomList.getString(7);
				String roomBuildingNr = roomList.getString(8);
	
				Room currentRoom = new Room(roomNr, roomName, capacity, roomType, buildingName, plz, street, roomBuildingNr);
				rooms.add(currentRoom);

			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		return rooms;
		
	}
	
	/**
	 * Method allows to insert information of results attendees achieved in an event into a database. Note that this information may only be added to {@code Exam} objects and {@code Lecture} (in case the lecture is {@code examImmanent}
	 * @param studentResults Identifiers of students and the results these students achieved in a class. Given numerically graded from 1 to 5 with 1 being the best grade.
	 * @param eventID Identifier of the event objects results are added for.
	 */
	public void setResults(Integer[][] studentResults, String eventID){
		String sql, sql1, sql2, sql3;
		String lectureType = null, eventDate = null, semester = null, LVNr = null; 
		Date date = new Date();
		ResultSet event;
		
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;

		System.out.println("CALL setResults");
		
		try {
			establishConnection();
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			
			sql = "SELECT typ FROM Event WHERE eventID='"+eventID+"'";
			System.out.println(sql); //DEBUG
			ResultSet type = stmt.executeQuery(sql);
			type.next();
			lectureType = type.getNString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(lectureType); //DEBUG
		if(lectureType.equals("exam")){
			try {
				
				sql1 = "SELECT examDate, semester, LVNr FROM Exam NATURAL JOIN event WHERE eventID='"+eventID+"'";
				System.out.println(sql1); //DEBUG
				event = stmt.executeQuery(sql1);
				event.next();
				eventDate = event.getNString(1);
				semester = event.getNString(2);
				LVNr = event.getString(3);
				
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");						
				
				try {
					date = dateFormat.parse(eventDate);
					eventDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} else {
			if(lectureType.equals("lecture")){
				try {
					sql2 = "SELECT lastSession, semester, LVNr FROM Lecture WHERE eventID='"+eventID+"'";
					event = stmt1.executeQuery(sql2);
					event.next();
					eventDate = event.getNString(1);
					semester = event.getNString(2);
					LVNr = event.getString(3);
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");						
					
					try {
						date = dateFormat.parse(eventDate);
						eventDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		}
		System.out.println("studentResults.length:  " + studentResults.length);
		
		for(int i=0; i<studentResults.length; i++){
			sql3 = "INSERT INTO Results VALUES('"+eventID+"','"+semester+"','"+LVNr+"',TO_DATE('"+eventDate+"','DD.MM.YYYY'),'"+studentResults[i][0]+"','"+studentResults[i][1]+"')";	
			System.out.println(sql3);
			try {
				stmt2.executeUpdate(sql3);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		try {	
			stmt.close();
			stmt1.close();
			stmt2.close();

			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
		System.out.println("Database insert done");

	}
	
	/**
	 * Method allows to access a list of all results obtained by participants in an event.
	 * @param eventID Identifier of an event
	 * @return results students achieved in an event
	 */
	public ArrayList<String> getResultsOfEvent(String eventID) {
	String sql = "SELECT LVNr, name, semester, examDate, matrNr, grade, typ FROM Results NATURAL JOIN Event WHERE eventID = '"+eventID+"' ";
	String LVNr = null, name = null, semester = null, examDate = null, matrNr = null, grade = null;
	java.sql.Statement stmt = null;
	ResultSet results = null;
	ArrayList<String> eventResults = new ArrayList<String>();
	//establish connection
	try {
		
		if(!establishConnection()) return null;
		stmt = con.createStatement();
		results = stmt.executeQuery(sql);
		
	} catch (ServletException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
		try {
			while (results.next()) {
				if(!results.getString(7).equals("lecture") && !results.getString(7).equals("exam")){
					throw new IllegalArgumentException("There are no results for this type of event");
				}
				LVNr = results.getString(1);
				name = results.getString(2);
				semester = results.getString(3);
				examDate = results.getString(4);
				matrNr = results.getString(5);
				grade = results.getString(6);
				eventResults.add(LVNr);
				eventResults.add(name);
				eventResults.add(semester);
				eventResults.add(examDate);
				eventResults.add(matrNr);
				eventResults.add(grade);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			stmt.close();
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return eventResults;
	}
	
	/**
	 * Method translates the result of an SQL database query into {@code User} objects
	 * @param userList Result of database query containing information on multiple users.
	 * @return List of users returned by the SQL query in form of {@code User} objects.
	 */
	public ArrayList<User> parseUsers(ResultSet userList) {
		System.out.println("parseUsers called"); //DEBUG
		java.sql.Statement stmtParse=null;
		java.sql.Statement stmtParse1=null;
		java.sql.Statement stmtParse2=null;
		System.out.println("java.sql.statement initialized"); //DEBUG
		//establish connection
		try {
			
			if(!establishConnection()) return null;
			stmtParse = con.createStatement();;
			stmtParse1 = con.createStatement();
			stmtParse2 = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("going to initialize ArrayList<user>"); //DEBUG
		ArrayList<User> users = new ArrayList<User>();
		System.out.println("array list initialized"); //DEBUG
		//Identify event type, add relevant information for each event type
		try {
			
			String sql1, sql2, sql3, sql4, sql5;
			System.out.println("vor while");
			while(userList.next()){
				//Parse general User
				System.out.println("in while"); //DEBUG
				
				String userName = userList.getNString(1);
				String password = userList.getNString(2);
				String firstName = userList.getNString(3);
				String lastName = userList.getNString(4);
				String email = userList.getNString(5);
				String birthdate = userList.getNString(6);
				System.out.println("vor datum parse"); //DEBUG 	
				System.out.println(userName); //DEBUG
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Date birthday = new Date();
				
				try {
					birthday = dateFormat.parse(birthdate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("general User parsed"); //DEBUG
				
				sql1 = "SELECT matrNr FROM Student WHERE userName='"+userName+"'";
				ResultSet rs = stmtParse.executeQuery(sql1);
				System.out.println("vor rs.next");
				System.out.println("nach rs.next");
				
					Integer matrNr = null;
					while(rs.next()) {matrNr = rs.getInt(1);}
					if(matrNr != null){
						//get names of studies of student
						sql2 = "SELECT name FROM Student NATURAL JOIN Studies NATURAL JOIN Discipline WHERE matrNr='"+matrNr+"'";
						ResultSet disciplines = stmtParse1.executeQuery(sql2);
						ArrayList<String> studies = new ArrayList<String>();
						while(disciplines.next()){
							studies.add(disciplines.getNString(1));					
						}
						//Parse student
						Student user = new Student(userName, email, password, firstName, lastName, birthday, studies, matrNr);
						users.add(user);
	
					} else {
						System.out.println("in else parse"); //DEBUG
						sql3 = "SELECT lecturerID FROM Lecturer WHERE userName='"+userName+"'";
						ResultSet rs2 = stmtParse1.executeQuery(sql3);
						String lecturerID = null;
						while(rs2.next()){lecturerID = rs2.getNString(1);}
						System.out.println("string lecturerID:  " + lecturerID); //DEBUG
						if(lecturerID != null){
							System.out.println("parse Lecturer"); //DEBUG
							sql4 = "SELECT name FROM Lecturer NATURAL JOIN Teaches NATURAL JOIN Discipline WHERE lecturerID='"+lecturerID+"'";
							ResultSet disciplines = stmtParse2.executeQuery(sql4);
							ArrayList<String> teaches = new ArrayList<String>();
							while(disciplines.next()){
								teaches.add(disciplines.getNString(1));					
							}
							
							//parse lecturer
							Lecturer user = new Lecturer(userName, email, password, firstName, lastName, birthday, teaches, lecturerID);
							users.add(user);
							
						} else {
							sql5 = "SELECT statisticianID FROM Statistician WHERE userName='"+userName+"'";
							String statisticianID = null;
							ResultSet rs3 = stmtParse2.executeQuery(sql5);
							while(rs3.next()){statisticianID=rs3.getNString(1);}
														
							//parse Statistician
							Statistician user = new Statistician(userName, email, password, firstName, lastName, birthday, statisticianID); 
							users.add(user);
						}
					}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			stmtParse.close();
			stmtParse1.close();
			stmtParse2.close();
		//	closeConnection();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("return users in parseUsers"); //DEBUG
		return users;
	}
}