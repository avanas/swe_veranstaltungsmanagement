/**
 * 
 */
package DAOs;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;

import Events.*;
import Rooms.*;
import Users.*;


/**
 * The class is used to manage {@code User} objects in a database, access information from the database and get information on the involvement of user with events. 
 */
public class UserDAO extends DAO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	EventDAO eventDAO;

	
	public UserDAO(){
		this.eventDAO= new EventDAO();
	}
	
	/**
	 * This method checks if the entered {@code userName} exists in the database.
	 * @param username of the user currently in session and identifier of an User.
	 * @return The {@code userName} if it is found in the database and a String "username wrong" if not. 
	 */
	public String proofUsername(String userName){
		
		java.sql.Statement stmt=null;
		
		try {				
			if(!establishConnection()) return "username wrong";
			try {
				stmt = con.createStatement();
			} catch (SQLException e) {
				System.out.println("SQLException in proofUsername " + e);
				return "username wrong";
				//e.printStackTrace();
			}
		} catch (ServletException e1) {
			System.out.println("ServletException in proofUsername " + e1);
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println("IOException in proofUsername " + e1);
			e1.printStackTrace();
		}
		
		String sql;
		sql = "SELECT userName FROM Person WHERE Person.userName='"+userName+"'";
		ResultSet checkuserName=null;
		try {
			checkuserName=stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			while(checkuserName.next()){
				String resultusername=checkuserName.getString(1);
				
				if(resultusername.equals(userName)){
					return userName;
				}
			}
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		return "username wrong";
	}

	/**
	 * This method checks if the entered {@code password} exists in the database and belongs to the entered {@code userName}.
	 * @param userName of the user currently in session and identifier of an user.
	 * @param password for the given {@code userName}. It's used to protect the users account. 
	 * @return password if it's the correct {@code password} for this {@code userName} and the String "password wrong" if not.
	 */
	public String proofPassword(String userName, String password){
		
	java.sql.Statement stmt=null;
	
		try {				
			if(!establishConnection()) return "password wrong";
			try {
				stmt = con.createStatement();
			} catch (SQLException e) {
				System.out.println("SQLException" + e);
				return "password wrong";
				//e.printStackTrace();
			}
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String sql;
		sql = "SELECT password FROM Person WHERE Person.userName='"+userName+"' AND Person.password='"+password+"'";
		ResultSet checkpassword=null;
		try {
			checkpassword=stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			while(checkpassword.next()){
				String resultpass=checkpassword.getString(1);
				System.out.println(resultpass);
				
				if(resultpass.equals(password)){
					System.out.println("proofPassword - true");
					return resultpass;
				}
			}
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		System.out.println("proofPassword - false");
		return "password wrong";
	}
	
	/**
	 * This method saves an object {@code user} in the database and determines what user-type it is.
	 * @param user object of type User.
	 */
	public void saveUser(User user) {
	    System.out.println("saveUser called");
        java.sql.Statement stmt=null;
        java.sql.Statement stmt1=null;
        java.sql.Statement stmt2=null;
        java.sql.Statement stmt3=null;   
        java.sql.Statement stmt4=null;   
        java.sql.Statement stmt5=null;
       
        try {                     
               establishConnection();
               try {
                      stmt = con.createStatement();
                      stmt1 = con.createStatement();
                      stmt2 = con.createStatement();
                      stmt3 = con.createStatement();
                      stmt4 = con.createStatement();
                      stmt5 = con.createStatement();
               } catch (SQLException e) {
                      // TODO Auto-generated catch block
                      e.printStackTrace();
               }     
        } catch (ServletException e1) {
               e1.printStackTrace();
        } catch (IOException e1) {
               e1.printStackTrace();
        }
              
        System.out.println(user.getUsername());
        System.out.println(user.getEmail());
        System.out.println(user.getBirthday());
        //+user.getEmail()+"',TO_DATE('"+user.getBirthday()+"','DD.MM.YYYY'))");
        java.sql.Date sqlbirthdate = new java.sql.Date(user.getBirthday().getTime());
        System.out.println(sqlbirthdate);
        
        try { 																																																									
               stmt.executeUpdate("INSERT INTO Person (userName, password, firstName, lastName, email, birthdate) VALUES('"+user.getUsername()+"','"+user.getPassword()+"','"+user.getFirstName()+"','"+user.getLastName()+"','"+user.getEmail()+"',TO_DATE('"+sqlbirthdate+"','YYYY-MM-DD'))");
        } catch(Exception e) {
               System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle User\n");
               return;
        }
              
               if(user instanceof Lecturer){
            	   System.out.println(user.getUsername());

            	   Lecturer lecturer = (Lecturer) user;
            	   System.out.println(lecturer.getLecturerID());
            	   System.out.println(lecturer.getDiscipline());
            	   System.out.println(Integer.parseInt(lecturer.getDiscipline().get(0)));
                      try {
       
                             stmt1.executeUpdate("INSERT INTO Lecturer (userName, lecturerID) VALUES ('"+lecturer.getUsername()+"',"+lecturer.getLecturerID()+")");
 
                      } catch(Exception e) {
                      System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Lecturer");
                      return;
                      }
                      
                      
                      try {
                          
                          stmt2.executeUpdate("INSERT INTO Teaches (lecturerID, disciplineID) VALUES ('"+lecturer.getLecturerID()+"','"+Integer.parseInt(lecturer.getDiscipline().get(0))+"')");

                   } catch(Exception e) {
                   System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Teaches");
                   return;
                   }
                      
               }
                     
               if(user instanceof Student){

            	   System.out.println(user.getUsername());

            	   Student student = (Student) user;
            	   System.out.println(student.getMatrNr());
            	   System.out.println(student.getDiscipline());
            	   System.out.println(Integer.parseInt(student.getDiscipline().get(0)));
                      try {
       
                             stmt3.executeUpdate("INSERT INTO Student (userName, matrNr) VALUES ('"+student.getUsername()+"',"+student.getMatrNr()+")");
 
                      } catch(Exception e) {
                      System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Student\n");
                      return;
                      }
                      
                      
                      try {
                          
                          stmt4.executeUpdate("INSERT INTO Studies (matrNr, disciplineID) VALUES ('"+student.getMatrNr()+"','"+Integer.parseInt(student.getDiscipline().get(0))+"')");

                   } catch(Exception e) {
                   System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Studies");
                   return;
                   }
                      
               }
              
               if(user instanceof Statistician){
            	   System.out.println(user.getUsername());

            	   Statistician stat = (Statistician) user;
            	   System.out.println(stat.getUsername());

                      try {
       
                             stmt5.executeUpdate("INSERT INTO Statistician (userName, statID) VALUES ('"+stat.getUsername()+"',"+stat.getStatisticianID()+")");
 
                      } catch(Exception e) {
                      System.err.println("Fehler beim Einfuegen des Datensatzes: " + e.getMessage() + "in Tabelle Statistician\n");
                      }
               }
              
               closeConnection();
              
  }
	
	/**
	 * Method deletes an user from the database and all the corresponding information stored alongside.	
	 * @param userName identifier of the user.
	 */		
	public void deleteUser(String userName) {
		
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
		java.sql.Statement stmt2=null;
		java.sql.Statement stmt3=null;
		java.sql.Statement stmt4=null;
		java.sql.Statement stmt5=null;
		java.sql.Statement stmt6=null;
		java.sql.Statement stmt7=null;
		java.sql.Statement stmt8=null;
		java.sql.Statement stmt9=null;
				
		try {
			establishConnection();
			try {
				stmt = con.createStatement();
				stmt1 = con.createStatement();
				stmt2 = con.createStatement();
				stmt3 = con.createStatement();
				stmt4 = con.createStatement();
				stmt5 = con.createStatement();
				stmt6 = con.createStatement();
				stmt7 = con.createStatement();
				stmt8 = con.createStatement();
				stmt9 = con.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
		String sql, sql1, sql2, sql3, sql4;
		ResultSet resultUser;
		sql = "SELECT * FROM Person WHERE userName='"+userName+"';";
		resultUser = stmt.executeQuery(sql);		
		
		if((resultUser.getNString(1)).equals("Lecturer")){
			sql1 = "DELETE Lecturer WHERE Lecturer.userName='"+userName+"'; ";
			ResultSet lecturerID = stmt.executeQuery(("SELECT LecturerID FROM Lecturer WHERE Lecturer.userName='"+userName+"';"));
			stmt1.executeUpdate("DELETE Exam WHERE lecturerID='"+lecturerID+"'; ");
			stmt2.executeUpdate("DELETE Lecture WHERE lecturerID='"+lecturerID+"'; ");
			stmt3.executeUpdate("DELETE Event WHERE lecturerID='"+lecturerID+"'; ");
			stmt4.executeUpdate(sql1);
		}
		
		if(resultUser.getNString(1).equals("Student")){
			sql2 = "SELECT matrNr FROM Student WHERE Student.userName='"+userName+"';";
			ResultSet matrNr = stmt5.executeQuery(sql2);
			
			String sql5;
			sql5 = "SELECT eventID FROM Attending WHERE Attending.userName='"+userName+"';";
			ResultSet checkeventID=null;
			try {
				checkeventID=stmt6.executeQuery(sql5);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			String sql6;
			sql6 = "DELETE Attending.eventID, Attending.userName, Attending.semester WHERE Attending.eventID='"+checkeventID+"' AND Attending.userName='"+userName+"';";
			
			try {
				stmt7.executeQuery(sql6);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			sql3 = "DELETE Student WHERE matrNr='"+matrNr+"AND userName='"+userName+"';";
			stmt8.executeUpdate(sql3);	
		}
		
		sql4 = "DELETE Person WHERE userName='"+userName+"';";
		stmt9.executeUpdate(sql4);
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
	}
		
	
	/**
	 * Method to alter the user information stored in the databse.
	 * @param userName used to identify the user.
	 * @param firstName the first name of the user.
	 * @param lastName the last name of the user.
	 * @param email the email address of the user.
	 * @param birthday the birthday of the user.
	 */
	public void editUser(String userName, String firstName, String lastName, String email, String birthday) {
		
		java.sql.Statement stmt=null;
		
		
		try {			
			establishConnection();
			try {
				stmt = con.createStatement();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String sql;
		sql = "UPDATE Person SET firstName='"+firstName+"', lastName='"+lastName+"', email='"+email+"', birthday='"+birthday+"' WHERE userName='"+userName+"';";

		try {
			stmt.executeUpdate(sql);	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		closeConnection();
	}
	
	/**
	 * Method to identify if an user is of the type Lecturer, Student or Statistician.	
	 * @param userName used to identify the user.
	 * @return String with the correct user type.
	 */	
public String getUserType(String userName){
		
	java.sql.Statement stmt=null;
	java.sql.Statement stmt1=null;
	java.sql.Statement stmt2=null;
	
	try {				
		establishConnection();
		
		try {
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (ServletException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
	
	String sql, sql1, sql2;
	sql = "SELECT userName FROM Student WHERE Student.userName='"+userName+"'";
	ResultSet checkuserName=null;
	try {
		checkuserName=stmt.executeQuery(sql);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName.next()){
			String resultusername=checkuserName.getString(1);
			
			if(resultusername.equals(userName)){
				return "student";
			}
		}
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	sql1 = "SELECT userName FROM Lecturer WHERE Lecturer.userName='"+userName+"'";
	ResultSet checkuserName1=null;
	try {
		checkuserName1=stmt1.executeQuery(sql1);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName1.next()){
			String resultusername=checkuserName1.getString(1);
			
			if(resultusername.equals(userName)){
				return "lecturer";
			}
		}
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	sql2 = "SELECT userName FROM Statistician WHERE Statistician.userName='"+userName+"'";
	ResultSet checkuserName2=null;
	try {
		checkuserName2=stmt2.executeQuery(sql2);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName2.next()){
			String resultusername=checkuserName2.getString(1);
			
			if(resultusername.equals(userName)){
				return "statistician";
			}
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	closeConnection();
	return null;
	}

/**
 * Method to get the University ID of the user depending on the user type.
 * @param userName used to identify the user.
 * @return the correct ID.
 */
public String getUserID(String userName){
	
	java.sql.Statement stmt=null;
	java.sql.Statement stmt1=null;
	java.sql.Statement stmt2=null;
	
	try {				
		establishConnection();
		
		try {
			stmt = con.createStatement();
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (ServletException e1) {
		e1.printStackTrace();
	} catch (IOException e1) {
		e1.printStackTrace();
	}
	
	String sql, sql1, sql2;
	sql = "SELECT matrnr FROM Student WHERE Student.userName='"+userName+"'";
	ResultSet checkuserName=null;
	try {
		checkuserName=stmt.executeQuery(sql);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName.next()){
			String resultuserID=checkuserName.getString(1);
			
				return resultuserID;
			
		}
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	sql1 = "SELECT lecturerID FROM Lecturer WHERE Lecturer.userName='"+userName+"'";
	ResultSet checkuserName1=null;
	try {
		checkuserName1=stmt1.executeQuery(sql1);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName1.next()){
			String resultuserID=checkuserName1.getString(1);
			
				return resultuserID;
			
		}
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	sql2 = "SELECT statID FROM Statistician WHERE Statistician.userName='"+userName+"'";
	ResultSet checkuserName2=null;
	try {
		checkuserName2=stmt2.executeQuery(sql2);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		while(checkuserName2.next()){
			String resultuserID=checkuserName2.getString(1);
			
				return resultuserID;
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	closeConnection();
	return null;
	}



/**
 * Method to unsubscribe an user from attending an event.
 * @param userName used to identify the user.
 * @param eventID used to identify the event.
 */
	public void unsubscribeEvent(String userName, String eventID){
	
		java.sql.Statement stmt=null;
		java.sql.Statement stmt1=null;
	
		try {				
			try {
				establishConnection();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				stmt = con.createStatement();
				stmt1 = con.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		} catch (ServletException e1) {
			e1.printStackTrace();
			}
		
		String sql;
		sql = "SELECT eventID FROM Attending WHERE Attending.userName='"+userName+"'";
		System.out.println(sql);
		ResultSet checkeventID=null;
		String checkedeventID = null;
		try {
			checkeventID=stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while(checkeventID.next()){
				checkedeventID=checkeventID.getString(1);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	
		
		String sql1;
		sql1 = "DELETE FROM Attending WHERE Attending.eventID='"+checkedeventID+"' AND Attending.userName='"+userName+"'";
		System.out.println(sql1);
		try {
			stmt1.executeQuery(sql1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		closeConnection();
	
		
	}
	
	/**
	 * Method to subscribe a user to attending an event.
	 * @param eventID used to identify the event.
	 * @param userName used to identify the user.
	 * @param semester contains the semester in which the event will be held.
	 */
	public void subscribeEvent(String eventID, String userName, String semester){
		
		java.sql.Statement stmt=null;
		
		try {				
			establishConnection();
			try {
				stmt = con.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String sql;
		sql = "INSERT INTO Attending (userName, eventID, semester) VALUES('"+userName+"','"+eventID+"','"+semester+"')";
		System.out.println(sql);
		
		try {
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		closeConnection();
	}
	
	/**
	 * Method checks if an user is subscribed to an event.
	 * @param eventID used to identify the event.
	 * @param userName used to identify the user.
	 * @return true if the user is subscribed and false if not.
	 */
	public boolean checkEventRegistration(String eventID, String userName){
		
		java.sql.Statement stmt=null;
		
		try {				
			establishConnection();
			try {
				stmt = con.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String sql;
		sql = "SELECT username FROM Attending WHERE Attending.eventID='"+eventID+"'";
		System.out.println(sql);
		ResultSet checkeventID=null;
		try {
			checkeventID=stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			while(checkeventID.next()){
				String resultusername=checkeventID.getString(1);
            if(resultusername.equals(userName)){
            	System.out.println(resultusername);
            	closeConnection();
            	return true;
            }     
        }
		} catch (SQLException e2) {
             // TODO Auto-generated catch block
             e2.printStackTrace();
		}
		closeConnection();
		return false;	
	}
	

	
	/**
	 * Method to get all events of an user for a specific day of the week ordered by their starting time.
	 * @param userName used to identify the user.
	 * @param weekday of which the events shall be shown.
	 * @return all events of that day.
	 */
public ArrayList<Event> getTimeTable(String userName, String weekday){
		java.sql.Statement stmt=null;
		String usertype = this.getUserType(userName);
		
		try {
			
			establishConnection();
			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ResultSet eventListbyWeekDay = null;
		
		//Identify all events
		
		if(usertype.equals("student")){
			String sql1 = "SELECT eventID, semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity FROM Event NATURAL JOIN Attending WHERE userName = '"+userName+"' AND dayOfWeek = '"+weekday+"' AND Event.typ != 'exam' ORDER BY to_char(starttime, 'hh24miss') ASC";
	
			System.out.println(sql1);
			try {
				
				eventListbyWeekDay = stmt.executeQuery(sql1);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			String sql1 = "SELECT eventID, semester, Event.typ, Event.name, Event.firstSession, Event.lastSession, Event.dayOfWeek, Event.startTime, Event.duration, lecturerID, Event.disciplineID, Event.capacity FROM Event NATURAL JOIN Lecturer WHERE userName = '"+userName+"' AND dayOfWeek = '"+weekday+"' AND Event.typ != 'exam' ORDER BY to_char(starttime, 'hh24miss') ASC";
			
			System.out.println(sql1);
			try {
				
				eventListbyWeekDay = stmt.executeQuery(sql1);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Event> events = eventDAO.parseEvents(eventListbyWeekDay);
	
		closeConnection();

		return events;		
	}

/**
 * Method to evaluate the number of a rooms of a specific type being used.
 * @param roomType defines the type of rooms
 * @return returns the number of rooms.
 */
public StringBuffer evaluateLoadbyRoomType(String roomType){
		
		java.sql.Statement stmt=null;
	
		try {				
			establishConnection();
			stmt = con.createStatement();
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql;
		sql = "SELECT COUNT(roomID) FROM Room NATURAL JOIN takesPlace WHERE Room.typ='"+roomType+"'";
		ResultSet rooms = null;
		
		try {
			rooms = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		StringBuffer roombytype= new StringBuffer();
		try {
			int i=0;
			while(rooms.next()){
				roombytype.insert(i,rooms.getString(i+1));
           }     
		} catch (SQLException e2) {
            e2.printStackTrace();
		}
	
		System.out.println(roombytype); //DEBUG
		closeConnection();
		roombytype.toString();
		
		return roombytype;
		
	}
	
/**
 * Method to evaluate the number of a rooms of a specific capacity being used.
 * @param roomCapacity defines the capacity of the rooms.
 * @return the number of rooms.
 */
	public StringBuffer evaluateLoadbyCapacity(Integer roomCapacity){
		
		System.out.println(roomCapacity);
		
		java.sql.Statement stmt=null;
		
		try {				
			establishConnection();
			stmt = con.createStatement();
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql;
		sql = "SELECT COUNT(roomID) From Room NATURAL JOIN takesPlace WHERE Room.capacity<="+roomCapacity+"";
		ResultSet rooms = null;
		
		try {
			rooms = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		StringBuffer roombycap= new StringBuffer();
		try {
			int i = 0;
			while(rooms.next()){
					roombycap.insert(i,rooms.getString(i+1));
					i++;
	       } 
			
		} catch (SQLException e2) {
	        e2.printStackTrace();
		}
	
	
		closeConnection();
		
		return roombycap;
	}
	
	/**
	 * Method to evaluate the number of students by discipline.
	 * @param DisciplineID used to identify the discipline.
	 * @return number of students.
	 */
	public StringBuffer evaluateNrOfStudentsByDiscipline(String DisciplineID){
					java.sql.Statement stmt=null;
					java.sql.Statement stmt1=null;
					String sql1 = null;
					
					try {				
						establishConnection();
						stmt = con.createStatement();
						stmt1 = con.createStatement();
					} catch (ServletException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					
					
					sql1 = "SELECT COUNT(matrNr) FROM Studies WHERE DisciplineID='"+DisciplineID+"'";
					ResultSet studentsNumber=null;
					
					
					try {
						studentsNumber = stmt1.executeQuery(sql1);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					StringBuffer numberOfstudents= new StringBuffer();
					try {
						int i = 0;
						while(studentsNumber.next()){
							numberOfstudents.insert(i,studentsNumber.getString(i+1));
							i++;
			           }     
					} catch (SQLException e2) {
			            e2.printStackTrace();
					}
				
					System.out.println(numberOfstudents); //DEBUG
					
					closeConnection();
					
					return numberOfstudents;
				
				

	}
	
	/**
	 * Method to evaluate the average event results of students by lecturer.
	 * @param LecturerID identifies the lecturer.
	 * @return the average exam result.
	 */
	public float evaluateResultsByLecturer(String LecturerID){
		
		java.sql.Statement stmt=null;
		
		try {				
			establishConnection();
			try {
				stmt = con.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String sql;
		float resultavg = 0;
		sql = "SELECT AVG(grade) FROM Results NATURAL JOIN event WHERE lecturerID='"+LecturerID+"'";
		ResultSet resultsetavg = null;
		System.out.println(sql);//DEBUG
		
		try {
			resultsetavg = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			while (resultsetavg.next()){
				try {
					resultavg = resultsetavg.getFloat(1);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("ResultAVG: " + resultavg); //DEBUG
		
		
		
		try {
			resultsetavg.close();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		closeConnection();
		
		return resultavg;
	
	}

	
	/**
	 * Method to get the users result of an event.
	 * @param username used to identify the user.
	 * @param eventID used to identify the event.
	 * @return the result.
	 */
	public Integer getResultOfUserByEvent(String username, String eventID){
	java.sql.Statement stmt=null;
		System.out.println("getResultsOfUserByEvent"); //DEBUG
		try {				
			establishConnection();	
			stmt = con.createStatement();
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String sql;
		sql = "SELECT * FROM Results NATURAL JOIN student NATURAL JOIN event WHERE eventID='"+eventID+"' AND username='"+username+"'";
		System.out.println(sql);
		ResultSet studentResult = null;
		
		try {
			studentResult = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ArrayList<String> Results = null;
		Results = parseResultSet(studentResult);

		Integer result = null;
		if (!Results.isEmpty()){
			result = Integer.parseInt(Results.get(4));
		}
		closeConnection();
		
		return result;
	
	}
	
	
	/**
	 * Method to return an ArrayList of a users event result.
	 * @param userName used to identify the user.
	 * @return the ArrayList of the results.
	 */
	public ArrayList<String> getResultsOfUser(String userName){
		System.out.println("getResultsOfUser in UserDAO called"); //DEBUG
		java.sql.Statement stmt=null;
		
		try {				
			establishConnection();	
			stmt = con.createStatement();
		} catch (ServletException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String sql;
		sql = "SELECT * FROM Results NATURAL JOIN student NATURAL JOIN event WHERE Student.username='"+userName+"'";
		ResultSet studentResult = null;
		System.out.println(sql);
		try {
			studentResult = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<String> Results = parseResultSet(studentResult);
		closeConnection();
		
		return Results;
	
	}
	
	
	/**
	 * Method gets list of lecturers of a given discipline
	 * @param Discipline of lecturers given by disciplineID
	 * @return List of {@code User} objects
	 */
	public ArrayList<User> getLecturerList(String disciplineID){
		ResultSet lecturers = null;
		java.sql.Statement stmt=null;
		try {
			
			establishConnection();
			
			stmt = con.createStatement();
			
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String sql;
		
		sql = "SELECT username, password, firstName, lastName, email, birthdate FROM lecturer NATURAL JOIN person NATURAL JOIN teaches WHERE disciplineID = '"+disciplineID+"'";
		
		System.out.println(sql); //DEBUG
		
		try {

			lecturers = stmt.executeQuery(sql);

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		System.out.println("call parseUsers"); //DEBUG
		ArrayList<User> userList = new ArrayList<User>();
		userList = eventDAO.parseUsers(lecturers);
		try {
			stmt.close();
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("return userList in getLecturerList in UserDAO!"); //DEBUG
		return userList;
	}
	
	
	/**
	 * Method to parse results of the method {@code getResultsOfUser}.
	 * @param studentResult results of events of an user
	 * @return parsed results.
	 */
	public ArrayList<String> parseResultSet(ResultSet studentResult) {
		System.out.println("parseResultSet in UserDAO called"); //DEBUG
		java.sql.Statement stmt=null;
		try {
			
			establishConnection();
			stmt = con.createStatement();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<String> Results = new ArrayList<String>();
		
		try {
				while(studentResult.next()){
					String semester = studentResult.getNString(2);
					Results.add(semester);
					
					String eventName = studentResult.getNString(9);
					Results.add(eventName);
					
					String eventID = studentResult.getNString(1);
					Results.add(eventID);
	
					String examDate = studentResult.getNString(5);
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");						
					Date date = new Date();
	
					try {
						date = dateFormat.parse(examDate);
						examDate = new SimpleDateFormat("dd.MM.yyyy").format(date);
						System.out.println("EXAMDATE");
						System.out.println(examDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					Results.add(examDate);
					String grade = studentResult.getNString(6);
					Results.add(grade);
				}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        if (stmt != null) { try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} }
	    }

			
		closeConnection();
		System.out.println("return Results");
		return Results;
	}
}



