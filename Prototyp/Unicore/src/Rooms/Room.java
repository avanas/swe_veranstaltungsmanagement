/**
 * 
 */
package Rooms;

/**
 * @author vmb
 *
 */
public class Room {
	
	/**
	 * 
	 */
	String roomNr;
	String roomName;
	Integer roomCapacity;
	String roomType;
	String roomBuildingName;
	String plz;
	String street;
	String roomBuildingNr;
	
	/**
	 * @param roomNr Identifier of a room, unique in a specific building
	 * @param roomName Name of the room
	 * @param roomCapacity Maximum number of participants allowed for events taking place in this room
	 * @param roomType Specifies the type of {@code Room}. Each room type is associated with certain materials available and thus relevant for selecting a room as location for an {@code Event}
	 * @param roomBuildingName Name of the building in which the room is located
	 * @param plz postal code
	 * @param street address of the building in which the room is located
	 * @param roomBuildingNr Specifies the building in which the {@code Room} is located through an unique identifier.

	 */


	
	public Room(String roomNr, String roomName, Integer roomCapacity, String roomType, String roomBuildingName, String plz, String street, String roomBuildingNr){
		setRoomNr(roomNr);
		setRoomName(roomName);
		setRoomCapacity(roomCapacity);
		setRoomType(roomType);
		setRoomBuildingName(roomBuildingName);
		setPlz(plz);
		setStreet(street);
		setRoomBuildingNr(roomBuildingNr);
		
	}

	/**
	 * @return Identifier of a room, unique in a specific building
	 */
	public String getRoomNr() {
		return roomNr;
	}

	/**
	 * @param roomNr Identifier of a room, unique in a specific building
	 */
	public void setRoomNr(String roomNr) {
		this.roomNr = roomNr;
	}

	/**
	 * @return name of a room
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return maximum number of participants allowed for events in a room
	 */
	public Integer getRoomCapacity() {
		return roomCapacity;
	}

	/**
	 * @param roomCapacity the roomCapacity to set
	 */
	public void setRoomCapacity(Integer roomCapacity) {
		this.roomCapacity = roomCapacity;
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return Building the room is located in
	 */
	public String getRoomBuildingName() {
		return roomBuildingName;
	}

	/**
	 * @param roomBuildingName the name of the building in which the room is located
	 */
	public void setRoomBuildingName(String roomBuildingName) {
		this.roomBuildingName = roomBuildingName;
	}

	/**
	 * 
	 * @return postal code
	 */
	public String getPlz() {
		return plz;
	}

	/**
	 * 
	 * @param plz postal code
	 */
	public void setPlz(String plz) {
		this.plz = plz;
	}

	/**
	 * 
	 * @return name of street, address
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * 
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
	/**
	 * 
	 * @return RoomBuildingNr
	 */
	public String getRoomBuildingNr(){
		return roomBuildingNr;
	}
	
	/**
	 * 
	 * @param RoomBuildingNr unique identifier
	 */
	public void setRoomBuildingNr(String roomBuildingNr){
		this.roomBuildingNr = roomBuildingNr;
	}
	
}
