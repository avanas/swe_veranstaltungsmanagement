/**
 * 
 */
package Events;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Event {
	
	/**
	 * Provides Objects containing information on general events as they are organized in the Unicore system.
	 */
	String eventName;
	String eventType;
	String eventDiscipline;
	Integer eventCapacity;
	String eventTime;
	Integer eventDuration;
	String eventLecturer;
	String semester;
	String eventFirstSession;
	String eventLastSession;
	String dayOfWeek;
	String eventID;

	/**
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. 
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Event} through an unique identifier.
	 * @param firstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param lastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param dayOfWeek Specifies the day at which the {@code Event} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code eventFirstSession}
	 * @param eventID Unique identifier of {@code Event} objects.
	 */

	public Event(String semester, String eventType, String eventName, String eventDiscipline, Integer eventCapacity, String eventTime, Integer eventDuration, String eventLecturer, String firstSession , String lastSession, String dayOfWeek, String eventID){
		setEventName(eventName);
		setEventType(eventType);
		setEventDiscipline(eventDiscipline);
		setEventCapacity(eventCapacity);
		setEventTime(eventTime);
		setEventDuration(eventDuration);
		setEventLecturer(eventLecturer);
		setSemester(semester);
		setEventFirstSession(firstSession);
		setEventLastSession(lastSession);
		setDayOfWeek(dayOfWeek);
		setEventID(eventID);

	}

	/**
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. 
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Event} through an unique identifier.
	 * @param firstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param lastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param eventID Unique identifier of {@code Event} objects.
	 */
	public Event(String semester, String eventType, String eventName, String eventDiscipline, Integer eventCapacity, String eventTime, Integer eventDuration, String eventLecturer, String firstSession , String lastSession, String eventID){
		setEventName(eventName);
		setEventType(eventType);
		setEventDiscipline(eventDiscipline);
		setEventCapacity(eventCapacity);
		setEventTime(eventTime);
		setEventDuration(eventDuration);
		setEventLecturer(eventLecturer);
		setSemester(semester);
		setEventFirstSession(firstSession);
		setEventLastSession(lastSession);
		findDayOfWeek(firstSession);
		setEventID(eventID);

	}
	
	/**
	 * @return eventName Name describing the {@code Event}
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName Name describing the {@code Event}
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	/**
	 * @return semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start.  
	 */
	public String getSemester(){
		return semester;
	}
	
	/**
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 */
	public void setSemester(String semester){
		this.semester = semester;
	}
	
	/**
	 * @return eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. 
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @param eventType Specifies the kind of {@code Event}. Note that the terms "lecture" and "exam" should be reserved for the respective classes Lecture and Exam. 
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	/**
	 * @return eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.

	 */
	public String getEventDiscipline() {
		return eventDiscipline;
	}

	/**
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 */
	public void setEventDiscipline(String eventDiscipline) {
		this.eventDiscipline = eventDiscipline;
	}

	/**
	 * @return eventCapacity Maximum number of participants allowed
	 */
	public Integer getEventCapacity() {
		return eventCapacity;
	}

	/**
	 * @param eventCapacity Maximum number of participants allowed
	 */
	public void setEventCapacity(Integer eventCapacity) {
		this.eventCapacity = eventCapacity;
	}

	/**
	 * @return eventTime Time at which the event starts
	 */
	public String getEventTime() {
		return eventTime;
	}

	/**
	 * @param eventTime Time at which the event starts
	 */
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	/**
	 * @return eventDuration time specified in minutes
	 */
	public Integer getEventDuration() {
		return eventDuration;
	}

	/**
	 * @param eventDuration time specified in minutes
	 */
	public void setEventDuration(Integer eventDuration) {
		this.eventDuration = eventDuration;
	}

	/**
	 * @return eventLecturer Identifier of the {@code User} responsible for the {@code Lecture}
	 */
	public String getEventLecturer() {
		return eventLecturer;
	}

	/**
	 * @param eventLecturer Identifier of the {@code User} responsible for the {@code Lecture}
	 */
	public void setEventLecturer(String eventLecturer) {
		this.eventLecturer = eventLecturer;
	}

	/**
	 * @return eventFirstSession Date at which the {@code Event} takes place for the first time.
	 */
	public String getEventFirstSession() {
		return eventFirstSession;
	}

	/**
	 * @param eventFirstSession Date at which the {@code Event} takes place for the first time.
	 */
	public void setEventFirstSession(String eventFirstSession) {
		this.eventFirstSession = eventFirstSession;
	}

	/**
	 * @return eventLastSession Date at which the {@code Event} takes place for the last time or date less than a week later.
	 */
	public String getEventLastSession() {
		return eventLastSession;
	}

	/**
	 * @param eventLastSession Date at which the {@code Event} takes place for the last time or date less than a week later.
	 */
	public void setEventLastSession(String eventLastSession) {
		this.eventLastSession = eventLastSession;
	}
	
	/** 
	 * @return dayOfWeek Day of week at which the {@code Event} takes place
	 */
	public String getDayOfWeek() {
		return dayOfWeek;
	}

	/**
	 * @param dayOfWeek Day of week at which the {@code Event} takes place
	 */
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	
	/**
	 * Determines the day of the week at which the event takes place based on the firstSession if the dayOfWeek is not explicitly given
	 * @param event
	 */
	public void findDayOfWeek(String event) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date dateEvent = new Date();
		try {
			dateEvent = dateFormat.parse(event);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar day = Calendar.getInstance();
		day.setTime(dateEvent);
		String dayOfWeek = day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
		setDayOfWeek(dayOfWeek);
	}
	
	/**
	 * @return eventID Unique identifier of an {@code Event}
	 */
	public String getEventID(){
		return eventID;
	}
	
	/**
	 * @param eventID Unique identifier of an {@code Event}
	 */
	public void setEventID(String eventID){
		this.eventID = eventID;
	}
}
