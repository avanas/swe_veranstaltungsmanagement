/**
 * 
 */
package Events;

public class Lecture extends Event{
	
	/**
	 * Instances of {@code Lecture} describe specialized {@code Event} types.
	 */
	Integer examImmanent;
	String lectureType;
	String lvnr;
	
	/**
	 * Method used to create {@code Lecture} objects. 
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Event} through an unique identifier.
	 * @param eventFirstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param eventLastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param dayOfWeek Specifies the day at which the {@code Event} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code eventFirstSession}
	 * @param examImmanent Specifies whether presence during the event described in the {@code Lecture} is mandatory for participants. Value of 1 means the presence is mandatory, value of 0 means presence is not required.
	 * @param lectureType Describes the type of {@code Lecture}, e.g. "UE", "VO", "VU".
	 * @param eventID Unique identifier of {@code Event} objects.
	 */
	
	
	
	public Lecture(String eventName, String semester, String lectureType, String eventDiscipline, Integer eventCapacity, 
			String eventTime, Integer eventDuration, String eventLecturer, Integer examImmanent, String first, String last, String dayOfWeek, String eventID, String lvnr) {		
		super(semester, "lecture", eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, first, last, dayOfWeek, eventID);
		setExamImmanent(examImmanent);
		setLectureType(lectureType);
		setLVNR(lvnr);
	}

	
	/**
	 * Method used to create {@code Lecture} objects. 
	 * @param semester Specifies the semester which the {@code Event} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventDiscipline Specifies the discipline the {@code Event} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Event}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Event} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Event} through an unique identifier.
	 * @param eventFirstSession Specifies the date at which the {@code Event} takes place for the first time. The date is specified in the format dd.MM.yyyy
	 * @param eventLastSession Specifies the date at which the {@code Event} takes place for the last time. The date is specified in the format dd.MM.yyyy. Note that the {@code eventLastSession} may describe a later date than the {@code eventFirstSession} as well as the same date but cannot be earlier than the {@code eventFirstSession}. 
	 * @param examImmanent Specifies whether presence during the event described in the {@code Lecture} is mandatory for participants. Value of 1 means the presence is mandatory, value of 0 means presence is not required.
	 * @param lectureType Describes the type of {@code Lecture}, e.g. "UE", "VO", "VU".
	 * @param eventID Unique identifier of {@code Event} objects.
	 */
	public Lecture(String eventName, String semester, String lectureType, String eventDiscipline, Integer eventCapacity, 
			String eventTime, Integer eventDuration, String eventLecturer, Integer examImmanent, String eventFirstSession, String eventLastSession, String eventID, String lvnr) {		
		super(semester, "lecture", eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, eventFirstSession, eventLastSession, eventID);
		findDayOfWeek(eventFirstSession);
		setExamImmanent(examImmanent);
		setLectureType(lectureType);
		setLVNR(lvnr);
	}
	
	
	/**
	 * @return the examImmanent
	 */
	public Boolean isExamImmanent() {
		if(examImmanent == 1) return true;
		else return false;
	}

	/**
	 * @param examImmanent 
	 */
	public void setExamImmanent(Integer examImmanent) {
		this.examImmanent = examImmanent;
	}	
	

	/**
	 * 
	 * @return examImmanent
	 */
	public Integer getExamImmanent(){
		return examImmanent;
	}

	/**
	 * 
	 * @return lectureType
	 */
	public String getLectureType() {
		return lectureType;
	}

	/**
	 * 
	 * @param lectureType
	 */
	public void setLectureType(String lectureType) {
		this.lectureType = lectureType;
	}
	
	/**
	 * 
	 * @param LVNR
	 */
	public void setLVNR(String lvnr){
		this.lvnr = lvnr;
	}
	
	/**
	 * 
	 * @return LVNR
	 */
	public String getLVNR(){
		return lvnr;
	}

}
