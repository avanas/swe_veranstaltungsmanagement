/**
 * 
 */
package Events;

public class Exam extends Event{
	
	/**
	 * 
	 */
	String examLecture;
	String examDate;

	/**
	 * Method used to create {@code Exam} objects
	 * @param semester Specifies the semester which the {@code Exam} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventDiscipline Specifies the discipline the {@code Exam} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Exam}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Exam} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Exam} through an unique identifier.
	 * @param examDate Specifies the date at which the {@code Exam} takes place. The date is specified in the format dd.MM.yyyy
	 * @param dayOfWeek Specifies the day at which the {@code Exam} takes place abbreviated in 3 letters, i.e. Mon, Tue etc. The day of the week defined here must be equivalent to the day of the week implicitly given through the {@code examDate}.
	 * @param examLecture Specifies the Lecture the {@code Exam} belongs to through an unique identifier.
	 * @param eventID Unique identifier of {@code Event} objects.
	 */
	public Exam(String eventName, String semester, String examDate, String dayOfWeek, String eventDiscipline, Integer eventCapacity, String eventTime,
			Integer eventDuration, String eventLecturer, String examLecture, String eventID) {
		super(semester, "exam", eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examDate, examDate, dayOfWeek, eventID);
		setExamDate(examDate); 
		setExamLecture(examLecture);
	}
	
	/**
	 * Method used to create {@code Exam} objects
	 * @param semester Specifies the semester which the {@code Exam} belongs to. Note that semesters need to be given such that the first two letters encode which type of semester ("WS" or "SS") the {@code Event} takes place in followed by the year of the semester in 4 numbers. Note that "WS" start at October first and end at January 31st while "SS" start at March first and end at June 30th. "WS" should be encoded by the year in which they start. 
	 * @param eventName Name of the object. Must not be longer than 30 characters.
	 * @param eventDiscipline Specifies the discipline the {@code Exam} belongs to through the unique identifier of the discipline.
	 * @param eventCapacity Specifies the maximum number of participants in the {@code Exam}. Note that this number can only be at most as big as the capacity of the Room the event takes place in.
	 * @param eventTime Specifies the time at which the {@code Exam} starts in the format HH:mm
	 * @param eventDuration Specifies the duration of the event in minutes.
	 * @param eventLecturer Specifies the Lecturer responsible for the {@code Exam} through an unique identifier.
	 * @param examDate Specifies the date at which the {@code Exam} takes place. The date is specified in the format dd.MM.yyyy
	 * @param examLecture Specifies the Lecture the {@code Exam} belongs to through an unique identifier.
	 * @param eventID Unique identifier of {@code Event} objects.
	 */
	public Exam(String eventName, String semester, String examDate, String eventDiscipline, Integer eventCapacity, String eventTime,
			Integer eventDuration, String eventLecturer, String examLecture, String eventID) {
		super(semester, "exam", eventName, eventDiscipline, eventCapacity, eventTime, eventDuration, eventLecturer, examDate, examDate, eventID);
		findDayOfWeek(examDate);
		setExamDate(examDate); 
		setExamLecture(examLecture);
	}
	
	
	/**
	 * @return examLecture Identifier of the lecture the exam is related to
	 */
	public String getExamLecture() {
		return examLecture;
	}

	/**
	 * @param examLecture Identifier of the lecture the exam is related to
	 */
	public void setExamLecture(String examLecture) {
		this.examLecture = examLecture;
	}

	/**
	 * 
	 * @return examDate specified in the format dd.MM.yyyy
	 */
	public String getExamDate(){
		return examDate;
	}
	
	/**
	 * 
	 * @param examDate specified in the format dd.MM.yyyy
	 */
	public void setExamDate(String examDate){
		this.examDate = examDate;
	}
	
}
