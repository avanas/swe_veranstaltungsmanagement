<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
   <%@ page import="Events.Event" %>
   <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 ArrayList<Event> events_mon = (ArrayList<Event>) request.getAttribute("events_mon");
 ArrayList<Event> events_tue = (ArrayList<Event>) request.getAttribute("events_tue");
 ArrayList<Event> events_wed = (ArrayList<Event>) request.getAttribute("events_wed");
 ArrayList<Event> events_thu = (ArrayList<Event>) request.getAttribute("events_thu");
 ArrayList<Event> events_fri = (ArrayList<Event>) request.getAttribute("events_fri");
 ArrayList<Event> events_sat = (ArrayList<Event>) request.getAttribute("events_sat");

  if(currentUserStatus != "loggedin" ){
 		response.sendRedirect("index.jsp"); 
  }
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
          <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
              <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
            		
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <h4>GIT</h4>
          <p></p>
          <h4>Projekttagebücher</h4>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">

				<%if(!events_mon.isEmpty()){%>					
	  					<table>
	  					<thead>
	  					<tr><th>Montag</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					  	<c:forEach items="${events_mon}" var="mon" >
   							
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${mon.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${mon.eventID}"> ${mon.eventName} </a>
   							 </td>
   							</c:forEach>
	  					</tr>
	  					</tbody>
	  					</table>	
				<%}%>
	  					
	  					
				<%if(!events_tue.isEmpty()){%>	  					
	  					<table>
	  					<thead>
	  					<tr><th>Dienstag</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					  	<c:forEach items="${events_tue}" var="tue" >
   							
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${tue.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${tue.eventID}"> ${tue.eventName} </a>
   							 </td>
   							</c:forEach>
	  					</tr>
	  					</tbody>
	  					</table>
				<%};%>
	  					
	  					
				<%if(!events_wed.isEmpty()){%>	  						  					
	  					<table>
	  					<thead>
	  					<tr><th>Mittwoch</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					<c:forEach items="${events_wed}" var="wed" >
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${wed.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${wed.eventID}"> ${wed.eventName} </a>
   							 </td>
   							</c:forEach>
	  					
	  					</tr>	
	  					</tbody>
	  					</table>
				<%}%>
	  					
				<%if(!events_thu.isEmpty()){%>	  						  					
	  					<table>
	  					<thead>
	  					<tr><th>Donnerstag</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					<c:forEach items="${events_thu}" var="thu" >
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${thu.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${thu.eventID}"> ${thu.eventName} </a>
   							 </td>
   							</c:forEach>
	  					
	  					</tr>	
	  					</tbody>
	  					</table>
				<%}%>
	  					
				<%if(!events_fri.isEmpty()){%>	  						  						  					
	  					<table>
	  					<thead>
	  					<tr><th>Freitag</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					<c:forEach items="${events_fri}" var="fri" >
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${fri.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${fri.eventID}"> ${fri.eventName} </a>
   							 </td>
   							</c:forEach>
	  					
	  					</tr>	
	  					</tbody>
	  					</table>
				<%}%>
	  					
	  					
	  					
	  					
				<%if(!events_sat.isEmpty()){%>	  						  						  						  					
	  					<table>
	  					<thead>
	  					<tr><th>Samstag</th></tr></thead>
	  					<tbody>
	  					<tr>
	  					<c:forEach items="${events_sat}" var="sat" >
   							<td style="width:200px">
   							
  							<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${sat.eventTime}" var="time" />
  							<fmt:formatDate pattern="HH:mm" value="${time}" />
  							 
  							 <br>
  							 
   							 <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${sat.eventID}"> ${sat.eventName} </a>
   							 </td>
   							</c:forEach>
	  					
	  					</tr>	
	  					</tbody>
	  					</table>
				<%}%>
	  					
      
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
