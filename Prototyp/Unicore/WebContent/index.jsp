<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
      <nav>
      
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
              <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
            		
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <a href="https://bitbucket.org/avanas/swe_veranstaltungsmanagement"><h4>GIT</h4></a>
          <p></p>
          <a href="https://cewebs.cs.univie.ac.at/SWE/ws13/index.php?m=D&t=uebung&c=show&CEWebS_what=g7"><h4>Projekttagebücher</h4></a>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
                
      
      
        <h2>Veranstaltungen nach Studienrichtung anzeigen:</h2>
        
        
		 <form action="EventManagement" method="post">
		  <input type="hidden" name="formType" value="eventsbydiscipline" /> 
          <div class="form_settings">
			<p><span>Studium auswählen:</span><select id="id" name="disciplineID">
				<option value="33661">Astronomie</option>
				<option value="33630">Biologie</option>
				<option value="33662">Chemie</option>
				<option value="33617">Deutsche Philologie</option>
				<option value="190344">Englisch_LA</option>
				<option value="66803">Geschichte</option>
				<option value="33521">Informatik</option>
				<option value="66834">Molekulare Biologie</option>
				<option value="33621">Mathematik_Bsc</option>
				<option value="190406">Mathematik_LA</option>
				<option value="449">Pharmazie</option>
				<option value="33676">Physik</option>
				<option value="33641">Publizistik</option>
				<option value="101">Rechtswissenschaften</option>
				<option value="33526">Wirtschaftsinformatik</option></p>
				</select></p>
				
			 <p><span>Semester</span><select id="id" name="semester">
				<option value="WS2011">WS2011</option>
				<option value="SS2011">SS2011</option>
				<option value="WS2012">WS2012</option>
				<option value="SS2012">SS2012</option>
				<option value="WS2013">WS2013</option>
				<option value="SS2013">SS2013</option>
				<option value="WS2014">WS2014</option>
				<option value="SS2014">SS2014</option>
				<option value="WS2015">WS2015</option>
				<option value="SS2015">SS2015</option>								
				<option value="WS2016">WS2016</option>
				<option value="SS2016">SS2016</option>	
				<option value="WS2017">WS2017</option>
				<option value="SS2017">SS2017</option>	
												</select></p>
				
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_eventsbydiscipline" value="Veranstaltungen anzeigen" />
          </div>
        </form>
        <br> <br><br>
        
        
        <h2>Nach Veranstaltungen suchen:</h2>

        <form action="EventManagement" method="post">
          <input type="hidden" name="formType" value="eventsbyparameters" />        
          <div class="form_settings">
            <p><span>Veranstaltungsname</span><input type="text" name="eventname" value="" /></p>
            <p><span>Veranstaltungs_ID</span><input type="text" name="eventID" value="" /></p>
            <p><span>Studienrichtung</span><select id="id" name="disciplineID">
            	<option value=""> --Studienrichtung auswählen--</option>
				<option value="33661">Astronomie</option>
				<option value="33630">Biologie</option>
				<option value="33662">Chemie</option>
				<option value="33617">Deutsche Philologie</option>
				<option value="190344">Englisch_LA</option>
				<option value="66803">Geschichte</option>
				<option value="33521">Informatik</option>
				<option value="66834">Molekulare Biologie</option>
				<option value="33621">Mathematik_Bsc</option>
				<option value="190406">Mathematik_LA</option>
				<option value="449">Pharmazie</option>
				<option value="33676">Physik</option>
				<option value="33641">Publizistik</option>
				<option value="101">Rechtswissenschaften</option>
				<option value="33526">Wirtschaftsinformatik</option></p>
			</select>
            <p><span>Studienrichtung eingeben</span><input type="text" name="disciplineName" value="" /></p>
            
            
            <p><span>LV-Leiter Vorname</span><input type="text" name="lecturerFirstName" value="" /></p>
            <p><span>LV-Leiter Nachname</span><input type="text" name="lecturerLastName" value="" /></p>
<!--             <p><span>Lehrveranstaltungstyp</span><select id="id" name="lectureType"> -->
<!--             	<option value="">--Typ auswählen-- </option> -->
<!-- 				<option value="UE">UE</option> -->
<!-- 				<option value="VO">VO</option> -->
	
<!-- 				</select></p> -->
			<p><span>Semester</span><select id="id" name="semester">
				<option value="WS2011">WS2011</option>
				<option value="SS2011">SS2011</option>
				<option value="WS2012">WS2012</option>
				<option value="SS2012">SS2012</option>
				<option value="WS2013">WS2013</option>
				<option value="SS2013">SS2013</option>
				<option value="WS2014">WS2014</option>
				<option value="SS2014">SS2014</option>
				<option value="WS2015">WS2015</option>
				<option value="SS2015">SS2015</option>								
				<option value="WS2016">WS2016</option>
				<option value="SS2016">SS2016</option>	
				<option value="WS2017">WS2017</option>
				<option value="SS2017">SS2017</option>	
				</select></p>
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_eventsearch" value="Veranstaltungen suchen" />

          </div>
        </form>
        <br><br><br>
        

        <h2>Alle Veranstaltungen anzeigen:</h2>

        <form action="EventManagement" method="post">
          <input type="hidden" name="formType" value="allEvents" />
        	<div class="form_settings">
        		<p style="padding-top: 15px"><span>&nbsp;</span>
				<input class="submit" type="submit" name="submit_eventsall" value="Alle Veranstaltungen anzeigen" />

			</div>
        </form>
      
      
      
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
