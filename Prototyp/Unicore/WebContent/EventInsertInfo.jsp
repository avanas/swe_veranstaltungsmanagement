<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 
 if(currentUserStatus != "loggedin" && currentUserType != "lecturer"){
		response.sendRedirect("index.jsp"); 
}
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
 <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <h4>GIT</h4>
          <p></p>
          <h4>Projekttagebücher</h4>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
        
        
        <h2>Neue Veranstaltungen anlegen:</h2>
        <form name="eventinsertinfo" action="EventManagement" method="post" onsubmit="return validateForm()">
          <div class="form_settings">
             <input type="hidden" name="formType" value="EventDetails"/>
            <p><span>Name:</span><input type="text" name="eventname" value="" /></p>
            
            <p><span>Veranstaltungstyp</span><select id="id"  name="eventtype">
            	<option value="lecture">Lehrveranstaltung</option>
				<option name="exam" value="exam">Prüfung</option>
				<option value="other">Andere Veranstaltung</option>     
				
				</select>       
            </p>
            
				
            <p><span>max. Teilnehmerzahl:</span><input type="number" name="capacity" value="" /></p>  
            
            <p><span>Studienrichtung</span><select id="id" name="disciplineID">
				<option value="33661">Astronomie</option>
				<option value="33630">Biologie</option>
				<option value="33662">Chemie</option>
				<option value="33617">Deutsche Philologie</option>
				<option value="190344">Englisch_LA</option>
				<option value="66803">Geschichte</option>
				<option value="33521">Informatik</option>
				<option value="66834">Molekulare Biologie</option>
				<option value="33621">Mathematik_Bsc</option>
				<option value="190406">Mathematik_LA</option>
				<option value="449">Pharmazie</option>
				<option value="33676">Physik</option>
				<option value="33641">Publizistik</option>
				<option value="101">Rechtswissenschaften</option>
				<option value="33526">Wirtschaftsinformatik</option></p>
			</select>
			
			<div class="lectureclass">
			 <p><span>Prüfungsimmanent</span><select id="id"  name="primmanent">
            	<option value="1">Ja</option>
				<option value="0">Nein</option>     
				</select>       
            </p>
            </div>
             <p><span>LVNr:</span><input type="text" name="lvnr" value="" maxlength="7"/></p>
             
             <p>
             <div class="lectureclass">
             <span>Lehrveranstaltungstyp</span><select id="id"  name="lvtype">
            	<option value="UE">Übung</option>
				<option value="VO">Vorlesung</option>
				</select>  
			</div>	
				
             <p><span class="examclass">Nr der zugehörigen LV:</span><input class="examclass" type="text" name="examlecture" value="" maxlength="7"/></p>
             
            
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_getRoom" value="Veranstaltung anlegen" />

          </div>
        </form>
        <br><br><br>
        

      
      </div>
	</div>
</div>
  
<script type="text/javascript">
function validateForm(){
	

 
 field_var = document.forms["eventinsertinfo"]["eventname"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie einen Veranstaltungsnamen an!");
	 return false;
 }
 
 
 var field_id3 = document.forms["eventinsertinfo"]["examlecture"].value;
 
 
 var field_id2 = document.forms["eventinsertinfo"]["lvnr"].value;
 if (field_id2==null || field_id2=="")
 {
	 alert("Bitte geben Sie die gewünschte LVNr an!");
	 return false;
 }
 
 
 var field_id = document.forms["eventinsertinfo"]["capacity"].value;
 if (field_id==null || field_id=="")
 {
	 alert("Bitte geben Sie die gewünschte Teilnehmerzahl an!");
	 return false;
 }
 
 
 
var numericExpression = /^[0-9]+$/;
if (!field_id.match(numericExpression)){
    alert('Teilnehmerzahl muss eine Zahl sein');
	return false;
}  

if (!field_id2.match(numericExpression)){
    alert('LVNr muss eine Zahl sein');
	return false;
}  
 
if (!field_id3.match(numericExpression) && field_id3!=""){
    alert('Nr der zugehörigen LV muss eine Zahl sein');
	return false;
}  
 
 

return true;
}

</script>
   
         
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js">  </script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    
    
      
      $("select").change(function() {
    		 $("select option:selected").each(function() {
    			 var exam = $(".examclass");
    			 var lecture = $(".lectureclass");
    			 if ($(this).val() == "exam") {
    				 exam.fadeIn()
    				 lecture.hide()
    				 }
    			 if ($(this).val() == "other" || $(this).val() == "lecture") {
    				 exam.hide()
    				 lecture.fadeIn();
    				 }
    			 

    			 
    		 });
    	 });
      
    });
    

     
  </script>
</body>
</html>
