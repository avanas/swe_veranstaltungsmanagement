<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>

  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
 <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li class="selected"><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <a href="https://bitbucket.org/avanas/swe_veranstaltungsmanagement"><h4>GIT</h4></a>
          <p></p>
          <a href="https://cewebs.cs.univie.ac.at/SWE/ws13/index.php?m=D&t=uebung&c=show&CEWebS_what=g7"><h4>Projekttagebücher</h4></a>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
                
      
      
        <h2>Neuen Benutzer anlegen:</h2>
        

		
		 <form name="registeruser"  action="UserManagement" method="post" onsubmit="return validateForm()">
		  <input type="hidden" name="formType" value="registeruser" /> 
          <div class="form_settings">
			<p><span>Benutzertyp auswählen:</span><select id="id" name="userType">
				<option value="student">Student</option>
				<option value="lecturer">Lehrperson</option>
				<option value="statistician">Statistiker</option>
			</select></p>
			
			 <p><span>Benutzername</span><input type="text" name="username" maxlength="15" value="" /></p>
			 <p><span>Passwort</span><input type="password" name="password" maxlength="15" value="" /></p>
			 <p><span>Passwort wiederholen:</span><input type="password" name="confirmpassword" maxlength="15" value="" /></p>
			 <br>				 
          	 <p><span>Vorname</span><input type="text" name="firstname" maxlength="20" value="" /></p>
			 <p><span>Nachname</span><input type="text" name="lastname" maxlength="20" value="" /></p>
			 <p><span>E-Mail</span><input type="email" name="email" maxlength="35" value="" /></p>
			 <p><span>Geburtsdatum (DD.MM.YYYY)</span><input type="date" name="birthdate" value="" maxlength="10"/></p>
			
			<p><span>ID/MatrNr</span><input type="text" name="matrnr"  maxlength="6" value="" /></p>
			
			<p><span>Studium auswählen:</span><select id="id" name="disciplineID">
				<option value="33661">Astronomie</option>
				<option value="33630">Biologie</option>
				<option value="33662">Chemie</option>
				<option value="33617">Deutsche Philologie</option>
				<option value="190344">Englisch_LA</option>
				<option value="66803">Geschichte</option>
				<option value="33521">Informatik</option>
				<option value="66834">Molekulare Biologie</option>
				<option value="33621">Mathematik_Bsc</option>
				<option value="190406">Mathematik_LA</option>
				<option value="449">Pharmazie</option>
				<option value="33676">Physik</option>
				<option value="33641">Publizistik</option>
				<option value="101">Rechtswissenschaften</option>
				<option value="33526">Wirtschaftsinformatik</option></p>

			
				</select></p>
				
			<p style="padding-top: 15px"><span>&nbsp;</span>
			
			<input class="submit" type="submit" name="submit_newuser" value="Benutzer anlegen"/>
          </div>
        </form>
        <br> <br><br>
        </div>
        </div>
        
       
<script type="text/javascript">
function validateForm(){
	
	
var field_var=document.forms["registeruser"]["username"].value;
if (field_var==null || field_var=="")
  {
	  alert("Bitte geben Sie einen Benutzernamen an!");
	  return false;
  }
 
 field_var = document.forms["registeruser"]["password"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie ein Passwort an!");
	 return false;
 }
  
 field_var = document.forms["registeruser"]["confirmpassword"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte bestätigen Sie das Passwort!");
	 return false;
 }
 
 field_var = document.forms["registeruser"]["firstname"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie einen Vornamen an!");
	 return false;
 }
 
 field_var = document.forms["registeruser"]["lastname"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie einen Nachnamen an!");
	 return false;
 }
 
 field_var = document.forms["registeruser"]["email"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie eine E-Mail Adresse an!");
	 return false;
 }
 
 field_var = document.forms["registeruser"]["birthdate"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie ein Geburtsdatum an!");
	 return false;
 }
 
 var field_id = document.forms["registeruser"]["matrnr"].value;
 if (field_id==null || field_id=="")
 {
	 alert("Bitte geben Sie eine ID/MatrNr an!");
	 return false;
 }
 
 
 
var numericExpression = /^[0-9]+$/;
if (!field_id.match(numericExpression)){
    alert('ID/MatrNr muss eine Zahl sein');
	return false;
}  
 
 var field_var3 = document.forms["registeruser"]["password"].value;
 var field_var2 = document.forms["registeruser"]["confirmpassword"].value;

 
 if(field_var3 != field_var2)
 {
	alert("Passwörter stimmen nicht überein!");
	return false;
}
 
	if(!dateCheck(document.forms["registeruser"]["birthdate"])) return false;

	var field_email=document.forms["registeruser"]["email"].value;
	  var atpos=field_email.indexOf("@");
	  var dotpos=field_email.lastIndexOf(".");
	  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	    {
	    alert("Keine gültige E-Mail Adresse");
	    return false;
	    }
	  
return true;
}

function dateCheck(field){
	var allowBlank = true;
	var minYear = 1900;
	var maxYear = (new Date()).getFullYear();
	
	var errorMsg = "";

	re = /^(\d{1,2}).(\d{1,2}).(\d{4})$/;
		
	if(field.value != '') {
	      if(regs = field.value.match(re)) {
	        if(regs[1] < 1 || regs[1] > 31) {
	          errorMsg = "Ungültiger Tag: " + regs[1];
	        } else if(regs[2] < 1 || regs[2] > 12) {
	          errorMsg = "Ungültiger Monat: " + regs[2];
	        } else if(regs[3] < minYear || regs[3] > maxYear) {
	          errorMsg = "Ungültiges Jahr: " + regs[3] + " - muss zwischen " + minYear + " und " + maxYear + "liegen";
	        }
	      } else {
	        errorMsg = "Ungültiges Datumsformat: " + field.value;
	      }
	    } else if(!allowBlank) {
	      errorMsg = "Geben Sie ein Datum ein!";
	    }

	    if(errorMsg != "") {
	      alert(errorMsg);
	      field.focus();
	      return false;
	    }
	
	
	return true;
}
</script>
   
       
       
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
