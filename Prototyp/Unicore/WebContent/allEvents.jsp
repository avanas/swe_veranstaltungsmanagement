<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 %>
			

        



<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>
        </div>
      </div>
      <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">

      <div id="content">
       
       
    <h2>Veranstaltungen:</h2>


	<table width="500" >
		<tr>
			<th align="left"> Name </th>
			<th align="left"> Studienrichtung </th>
			<th align="left"> Typ </th>
			<th align="left"> Wochentag </th>
			<th align="left"> erste Einheit </th>
			<th align="left"> letzte Einheit </th>
			<th align="left"> Uhrzeit </th>
			<th align="left"> Dauer </th>
			<th align="left"> EventID </th>
			
			
		</tr>

		<c:forEach var="event" items="${events}"> 
			<tr>
			<td> ${event.eventName}</td>
			<td> ${event.eventDiscipline}</td>
			<td> ${event.eventType}</td>
			<td> ${event.dayOfWeek}</td>
			
			<td>
			 <fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${event.eventFirstSession}" var="time" />
  			<fmt:formatDate pattern="dd.MM.yyyy" value="${time}" />
			</td>
			
		
			
			<td>
			 <fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${event.eventLastSession}" var="time" />
  			<fmt:formatDate pattern="dd.MM.yyyy" value="${time}" />
			</td>
			
			
			<td>
			 <fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${event.eventTime}" var="time" />
  			<fmt:formatDate pattern="HH:mm" value="${time}" />
			</td>
			
			
			<td> ${event.eventDuration}</td>
			<td> ${event.eventID}</td>
			
			<td> <a href="EventManagement?pagerequest=eventdetail&amp;eventID=${event.eventID}">Ansicht</a></td>
			
			</tr>
		</c:forEach>
	</table>
		
	


      
      
      
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
