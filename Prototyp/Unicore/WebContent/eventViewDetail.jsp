<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
   <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 String currentUserID = (String) session.getAttribute("currentUserID");
 Boolean eventreg = (Boolean) request.getAttribute("eventreg");
 String eventTypeVar = (String) request.getAttribute("eventTypeVar");
 String eventLecturer = (String) request.getAttribute("eventLecturer");
 Boolean examImmanent = (Boolean) request.getAttribute("examImmanent");
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
          <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
              <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
            		
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <h4>GIT</h4>
          <p></p>
          <h4>Projekttagebücher</h4>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
                <table>
                	<thead>
                		<tr> <th>Detailansicht</th> </tr>
                	</thead>
                	
                	<tbody>
   						
   						<tr>
   						<td> Titel </td>
   						<td> ${eventdetail.eventName} </td>
   						</tr>
   						
   						<tr>
   						<td> Veranstaltungstyp </td>
   						<td> ${eventdetail.eventType} </td>
   						</tr>	
   						
   						<tr>
   						<td> Studienrichtung </td>
   						<td> ${eventdetail.eventDiscipline} </td>
   						</tr>
   						
   						<tr>
   						<td> max. Teilnehmerzahl </td>
   						<td> ${eventdetail.eventCapacity} </td>
   						</tr>
   						
   						<tr>
   						<td> Semester </td>
   						<td> ${eventdetail.semester} </td>
   						</tr>   						
 
   						<tr>
   						<td> Wochentag </td>
   						<td> ${eventdetail.dayOfWeek} </td>
   						</tr>
   						
   						<tr>
   						<td> erste Einheit </td>
   						<td>
   						<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${eventdetail.eventFirstSession}" var="time" />
  						<fmt:formatDate pattern="dd.MM.yyyy" value="${time}" />
   						</td>   						</tr>
   						
   						<tr>
   						<td> letzte Einheit </td>
   						<td>
   						<fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${eventdetail.eventLastSession}" var="time" />
  						<fmt:formatDate pattern="dd.MM.yyyy" value="${time}" />
   						</td>
   						</tr>   
   												   						  						
   						<tr>
   						<td> Zeit</td>
   						<td>
						 <fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${eventdetail.eventTime}" var="time" />
  						<fmt:formatDate pattern="HH:mm" value="${time}" />
						</td>
   						</tr>      							
       		
   						<tr>
   						<td> Dauer</td>
   						<td> ${eventdetail.eventDuration} min</td>
   						</tr>            		

   						<tr>
   						<td> </td>
   						<td>
  
   						    <% if (currentUserStatus == "loggedin" && currentUserType == "student") { 
   						    		if (eventreg) {%>  
           			 		<a href="UserManagement?pagerequest=unsubscribe&amp;eventID=${eventdetail.eventID}&amp;semester=${eventdetail.semester}"> Abmelden</a>
            				<%} else {%>
            					<a href="UserManagement?pagerequest=subscribe&amp;eventID=${eventdetail.eventID}&amp;semester=${eventdetail.semester}">Anmelden</a>
            				<% }
   						    		}; 
   						    	if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            				<a href="EventManagement?pagerequest=deleteEvent&amp;eventID=${eventdetail.eventID}&amp;semester=${eventdetail.semester}"> Löschen</a><br><br>
            					<%if ((eventTypeVar =="lecture" && examImmanent) || eventTypeVar=="exam") { if (eventLecturer.equals(currentUserID)) { %><a href="EventManagement?pagerequest=getAttendees&amp;eventID=${eventdetail.eventID}&amp;semester=${eventdetail.semester}"> Noten eintragen</a>           				
            				<%} }
            					};%>
            			</td>
   						</tr>  
   						
	             	</tbody>
                	</table>
   
      
      
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
