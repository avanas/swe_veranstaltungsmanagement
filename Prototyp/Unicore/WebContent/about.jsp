<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 %>
			

        
<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>
        </div>
      </div>
       <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li class="selected"><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">
        <div class="sidebar">
<!--           <h3>Latest News</h3> -->
<!--          <h4>New Website Launched</h4> -->
<!--           <h5>06.November.2013</h5> -->
<!--           <p>Start des Projekts unicore...<a href="#">read more</a></p> -->
        </div>
        <div class="sidebar">
          <h3>Teammitglieder</h3>
          <h4>Agathe Vanas</h4>
          <p>Matrikelnummer: 0946513 a0946513@unet.univie.ac.at</p>
          <h4>Christopher David Viertmann</h4>
          <p>Matrikelnummer: 1168316 a1168316@unet.univie.ac.at</p>
          <h4>Verena Maria Bauer</h4>
          <p>Matrikelnummer: 1021709 a1021709@unet.univie.ac.at</p>
          <h3>Projekt Tagebücher</h3>
          <p><a href="https://bitbucket.org/avanas/swe_veranstaltungsmanagement/wiki/Home">Zu den Tagebüchern</a></p>
        </div>
        <div class="sidebar">
<!--           <h3>Contact Us</h3> -->
<!--           <p>We'd love to hear from you. Call us, <a href="#">email us</a> or complete our <a href="contact.php">contact form</a>.</p> -->
        </div>
      </div>
      <div id="content">
        <h1>Willkommen bei unicore!</h1>
        <p>Ziel ist die Erstellung eines Organisationssystem im universitären Bereich. Dieses soll unterschiedlichen Benutzergruppen die Möglichkeit zur Verwaltung, Auswertung und Anmeldung zu verschiedenen Veranstaltungen bieten.</p>
        <p> Es gibt drei verschiedene Arten von Veranstaltungen:</p>
         <ul>
          <li>Lehrveranstaltungen</li>
          <li>Prüfungen</li>
          <li>sonstige Veranstaltungen</li>
         </ul>
        <h3>Lehrpersonen</h3>
        <p>Lehrpersonen haben die Möglichkeit nach Räumen zu suchen, die an einem bestimmten Zeitpunkt an der Universität verfügbar sind. Sie können dann diesen Raum für eine Veranstaltung buchen. Sie haben die Möglichkeit die Dauer der Veranstaltung anzugeben und zwischen einmaligen und wöchentlich stattfindenden Veranstaltungen zu unterscheiden. Bei Prüfungen besteht auÃerdem die Möglichkeit für jeden Teilnehmer eine Note einzutragen.</p>
        <h3>Studenten</h3>
        <p>Studenten können Veranstaltungen anhand des Namens, Ort oder Zeit suchen und sich dann für eine od. mehrere anmelden. Außerdem soll es für sie möglich sein eine Übersicht aller Veranstaltungen, für die sie angemeldet sind, in Form eines Stundenplans einzusehen, bei Prüfungen ist können sie zudem die jeweilige Note einsehen.</p>
      	<h3>Satistiker</h3>
      	<p>Statistiker werten die Belegungen von Veranstaltungen und Räumen aus. Sie haben die Möglichkeit Daten über die Auslastung von Räumen, die Teilnehmerzahlen einzelner Kurse und die Notenverteilung bei Prüfungen abzufragen.</p>
      	<h3>Weitere Informationen</h3>
      	<p>Weitere Informationen und Dokumente zu diesem Projekt finden sie unter:</p>
      	<p><a href="https://bitbucket.org/avanas/swe_veranstaltungsmanagement">https://bitbucket.org/avanas/swe_veranstaltungsmanagement</a></p>
      </div>
    </div>
    <div id="scroll">
      <a title="Scroll to the top" class="top" href="#"><img src="images/top.png" alt="top" /></a>
    </div>
<!--     <footer> -->
<!--       <p><a href="index.jsp">Home</a>  </p> -->
<!--       <p>Copyright &copy; CSS3_four | <a href="http://www.css3templates.co.uk">design from css3templates.co.uk</a></p> -->
<!--     </footer> -->
  </div>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
