<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 String loadbyroomtype = (String) session.getAttribute("loadbyroomtype");
 String loadbycapacity = (String) session.getAttribute("loadbycapacity");

 if(currentUserStatus != "loggedin" ){
		response.sendRedirect("index.jsp"); 
}
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
 <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            </li>
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <h4>GIT</h4>
          <p></p>
          <h4>Projekttagebücher</h4>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
                
      
      
      
        <h2>Auswertung nach Raumtyp:</h2>
        
        
		 <form action="UserManagement" method="post">
		  <input type="hidden" name="formType" value="statsbyroomtype" /> 
          <div class="form_settings">
			<p><span>Raumtyp auswählen:</span><select id="id" name="roomtype">
				<option value="HS">Hörsaal</option>
				<option value="EDV-Raum">PC-Raum</option>
				<option value="Lernzone">Lernzone</option>
				<option value="Bibliothek">Bibliothek</option>
				<option value="Mensa">Mensa</option>
				<option value="Seminarraum">Seminarraum</option>
				
				</select></p>
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_roomtype" value="Auslastung aufrufen" />
          </div>
        </form>
        <br> <br><br>
        
        
                           <% if (loadbyroomtype != null) { %>  
                           <h4>Anzahl der stattfindenden Veranstaltungen nach Raumtyp: </h4>
           			   <span><%= loadbyroomtype %></span> 
            		<%} %>
            		
            	<br> <br>
            	
            		
         <h2>Auswertung nach Kapazität:</h2>
        
        
		 <form action="UserManagement" method="post">
		  <input type="hidden" name="formType" value="statsbycapacity" /> 
          <div class="form_settings">
			<p><span>Kapazität eingeben:</span><input type="text" name="capacity" value="" /></p>  
				
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_capacity" value="Auslastung aufrufen" />
          </div>
        </form>
        <br> <br><br>
        
        
                           <% if (loadbycapacity != null) { %>  
                           <h4>Anzahl der stattfindenden Veranstaltungen in Räumen mit angegebener Kapazität od. kleiner : </h4>
           			   <span><%= loadbycapacity %></span> 
            		<%} %>           		
            		
            		
        
      
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
