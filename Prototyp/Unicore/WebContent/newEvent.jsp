<%@ page language="java"  contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"
         import="java.util.*"
   %>
   
   <%@ page import="Management.UserManagement" %>
    <%@ page import="javax.servlet.http.HttpServletRequest" %>
 
 
 <%
 String currentUser = (String) session.getAttribute("currentUser");
 String currentUserType = (String) session.getAttribute("currentUserType");
 String currentUserStatus = (String) session.getAttribute("status");
 
 if(currentUserStatus != "loggedin" ){
		response.sendRedirect("index.jsp"); 
}
 %>
			

        

<!DOCTYPE HTML>
<html>

<head>
  <title>unicore</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">unicore<span class="logo_colour"></span></a></h1>
          <h2>Die Universitätsplattform</h2>    
        </div>
      </div>
 <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li class="selected"><a href="index.jsp">Veranstaltungen</a></li>
            <li><a href="#">Mein Unicore</a>
                           <ul>
              	      <% if (currentUserStatus == "loggedin" && currentUserType == "student") { %>  
           			 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=myexams"> Meine Ergebnisse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "lecturer") {%>
            		 <li><a href="UserManagement?pagerequest=mycourses"> Meine Kurse</a> </li>
           			 <li><a href="UserManagement?pagerequest=timetable"> Stundenplan</a> </li>    			 
           			 <li><a href="newEvent.jsp"> Neue Veranstaltung </a> </li>
            		<%} if (currentUserStatus == "loggedin" && currentUserType == "statistician") {%>
            		 <li><a href="stat_load.jsp"> Raumauslastung</a> </li>
           			 <li><a href="stat_discipline.jsp"> Studienrichtungen </a> </li>   
           			 <li><a href="stat_results.jsp"> Notendurchschnitt </a> </li>              			         		
            		<%} if (currentUserStatus != "loggedin") { %>  
            		  <li><a href="login.jsp">Login</a></li>
            		<%}%>
            	
                </ul>
                
                </li>
            
            <li><a href="about.jsp">Über das Projekt</a></li>
                   
                   <% if (currentUserStatus== "loggedin") { %>  
           			 <li><a href="UserManagement?pagerequest=logout"> Logout</a> </li>
            		<%} else {%>
            		<li><a href="register.jsp">Registrieren</a></li>
            		<% }; %>
      
          </ul>
        </div>
      </nav>
    </header>
    <div id="site_content">
      <div id="sidebar_container">

        <div class="sidebar">
          <h3>Links</h3>
          <h4>GIT</h4>
          <p></p>
          <h4>Projekttagebücher</h4>
          <p></p>
          <h4>SWE</h4>
          <p></p>
          
        </div>
        
      </div>
      <div id="content">
      
        
        
        <h2>Neue Veranstaltungen anlegen:</h2>
        <form name="newevent" action="EventManagement" method="post" onsubmit="return validateForm()">
          <div class="form_settings">
             <input type="hidden" name="formType" value="checkRoom"/>
            <p><span>Datum der ersten Einheit:</span><input type="text" name="firstsession" value="" /></p>
            <p><span>Datum der letzten Einheit:</span><input type="text" name="lastsession" value="" /></p>
            
            <p><span>Semester</span><select id="id" name="semester">
				<option value="WS2011">WS2011</option>
				<option value="SS2011">SS2011</option>
				<option value="WS2012">WS2012</option>
				<option value="SS2012">SS2012</option>
				<option value="WS2013">WS2013</option>
				<option value="SS2013">SS2013</option>
				<option value="WS2014">WS2014</option>
				<option value="SS2014">SS2014</option>
				<option value="WS2015">WS2015</option>
				<option value="SS2015">SS2015</option>								
				<option value="WS2016">WS2016</option>
				<option value="SS2016">SS2016</option>	
				<option value="WS2017">WS2017</option>
				<option value="SS2017">SS2017</option>	
				</select></p>
            
            <p><span>Raumtyp</span><select id="id"  name="type">
            	<option value="Hoersaal">Hörsaal</option>
				<option value="PC-Raum">PC-Raum</option>     
				</select>       
            </p>
             <p><span>Raumkapazität:</span><input type="number" name="capacity" value="" /></p>  
             <p><span>Zeit: (HH:MM)</span><input type="text" name="eventtime" value="" /></p>
             <p><span>Dauer: (in min)</span><input type="text" name="eventduration" value="" /></p>
            
            
			<p style="padding-top: 15px"><span>&nbsp;</span>
			<input class="submit" type="submit" name="submit_getRoom" value="Räume suchen" />

          </div>
        </form>
        <br><br><br>
        

      
      </div>
	</div>
</div>

<script type="text/javascript">
function validateForm(){
	

 
 field_var = document.forms["newevent"]["firstsession"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie das Datum der ersten Einheit an!");
	 return false;
 }
 
 field_var = document.forms["newevent"]["lastsession"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie das Datum der letzten Einheit an!");
	 return false;
 }
 
 field_var = document.forms["newevent"]["eventtime"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie Uhrzeit an!");
	 return false;
 }
 
 field_var = document.forms["newevent"]["eventduration"].value;
 if (field_var==null || field_var=="")
 {
	 alert("Bitte geben Sie eine Veranstaltungsdauer an!");
	 return false;
 }
 
 
 var field_id = document.forms["newevent"]["capacity"].value;
 if (field_id==null || field_id=="")
 {
	 alert("Bitte geben Sie die gewünschte Raumkapazität an!");
	 return false;
 }
 
 
 
var numericExpression = /^[0-9]+$/;
if (!field_id.match(numericExpression)){
    alert('Kapazität muss eine Zahl sein');
	return false;
}  
 
 

 
	if(!dateCheck(document.forms["newevent"]["firstsession"])) return false;
	if(!dateCheck(document.forms["newevent"]["lastsession"])) return false;
	if(!checkTime(document.forms["newevent"]["eventtime"])) return false;
	  
return true;
}

function checkTime(field){
	  var errorMsg = "";

	    re = /^(\d{1,2}):(\d{2})(:00)?([ap]m)?$/;

	    if(field.value != '') {
	      if(regs = field.value.match(re)) {
	        if(regs[4]) {
	          // 12-hour time format with am/pm
	          if(regs[1] < 1 || regs[1] > 12) {
	            errorMsg = "Ungültige Angabe der Stunden: " + regs[1];
	          }
	        } else {
	          // 24-hour time format
	          if(regs[1] > 23) {
	            errorMsg = "Ungültige Angabe der Stunden: " + regs[1];
	          }
	        }
	        if(!errorMsg && regs[2] > 59) {
	          errorMsg = "Ungültige Angabe der Minuten: " + regs[2];
	        }
	      } else {
	        errorMsg = "Ungültige Zeitangabe: " + field.value;
	      }
	    }

	    if(errorMsg != "") {
	      alert(errorMsg);
	      field.focus();
	      return false;
	    }

	    return true;	
}

function dateCheck(field){
	var allowBlank = true;
	var minYear = 1900;
	var maxYear = (new Date()).getFullYear();
	
	var errorMsg = "";

	re = /^(\d{1,2}).(\d{1,2}).(\d{4})$/;
		
	if(field.value != '') {
	      if(regs = field.value.match(re)) {
	        if(regs[1] < 1 || regs[1] > 31) {
	          errorMsg = "Ungültiger Tag: " + regs[1];
	        } else if(regs[2] < 1 || regs[2] > 12) {
	          errorMsg = "Ungültiger Monat: " + regs[2];
	        } else if(regs[3] < minYear || regs[3] > maxYear) {
	          errorMsg = "Ungültiges Jahr: " + regs[3] + " - muss zwischen " + minYear + " und " + maxYear + "liegen";
	        }
	      } else {
	        errorMsg = "Ungültiges Datumsformat: " + field.value;
	      }
	    } else if(!allowBlank) {
	      errorMsg = "Geben Sie ein Datum ein!";
	    }

	    if(errorMsg != "") {
	      alert(errorMsg);
	      field.focus();
	      return false;
	    }
	
	
	return true;
}
</script>
   
         
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
      $('.top').click(function() {$('html, body').animate({scrollTop:0}, 'fast'); return false;});
    });
  </script>
</body>
</html>
