INSERT INTO Building VALUES(001, 'Informatik Institut', 1090, 'Waehringerstrasse', 29);

INSERT INTO Room VALUES(00001, 001, 'HS1', 'Hoersaal', 300);

INSERT INTO Discipline VALUES('033521', 'Informatik');
INSERT INTO Discipline VALUES('033577', 'Publizistik');
INSERT INTO Discipline VALUES('033661', 'Astronomie');
INSERT INTO Discipline VALUES('033630', 'Biologie');
INSERT INTO Discipline VALUES('033662', 'Chemie');
INSERT INTO Discipline VALUES('033617', 'Deutsch');
INSERT INTO Discipline VALUES('033612', 'Englisch');
INSERT INTO Discipline VALUES('033603', 'Geschichte');
INSERT INTO Discipline VALUES('033635', 'Kunstgeschichte');
INSERT INTO Discipline VALUES('033621', 'Mathematik');
INSERT INTO Discipline VALUES('033636', 'Musikwissenschaft');
INSERT INTO Discipline VALUES('033449', 'Pharmazie');
INSERT INTO Discipline VALUES('033676', 'Physik');
INSERT INTO Discipline VALUES('033101', 'Rechtswissenschaften');
INSERT INTO Discipline VALUES('033526', 'Wirtschaftsinformatik');


INSERT INTO Person VALUES('lecturer1', 'pw_lec1', 'Hans', 'Moritsch', 'hans.moritsch@univie.ac.at', TO_DATE('01.01.1980', 'DD.MM.YYYY'));
INSERT INTO Person VALUES('lecturer2', 'pw_lec2', 'Martina', 'Huber', 'martina.huber@univie.ac.at', TO_DATE('16.04.1982', 'DD.MM.YYYY'));

INSERT INTO Person VALUES('student1', 'pw_stu1', 'Max', 'Mustermann', 'maxi@123.com', TO_DATE('11.11.1990', 'DD.MM.YYYY'));
INSERT INTO Person VALUES('student2', 'pw_stu1', 'Thomas', 'Beck', 'tom.b@123.com', TO_DATE('04.03.1992', 'DD.MM.YYYY'));

INSERT INTO Person VALUES('stat1','pw_sta1','Maria','Musterfrau','maria@test.com',TO_DATE('01.02.1992','DD.MM.YYYY'));


INSERT INTO Lecturer VALUES('lecturer1', '87654321');
INSERT INTO Lecturer VALUES('lecturer2', '87654444');

INSERT INTO Student VALUES('student1', '0876543');
INSERT INTO Student VALUES('student2', '1060061');

INSERT INTO Statistician VALUES('stat1','084568');

INSERT INTO Teaches VALUES('87654321', '033521');
INSERT INTO Teaches VALUES('87654444', '033577');

INSERT INTO Studies VALUES('0876543', '033521');
INSERT INTO Studies VALUES('1060061', '033577');

INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'WS2013', TO_DATE('02.10.2013', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('19:30', 'HH24:MI'), 90);
INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'WS2013', TO_DATE('06.11.2013', 'DD.MM.YYYY'), TO_DATE('27.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('16:45', 'HH24:MI'), 90);
INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'WS2013', TO_DATE('29.01.2014', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('16:45', 'HH24:MI'), 90);
INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'WS2013', TO_DATE('08.10.2013', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Tue', TO_DATE('11:45', 'HH24:MI'), 120);
INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'WS2013', TO_DATE('19.12.2013', 'DD.MM.YYYY'), TO_DATE('19.12.2013', 'DD.MM.YYYY'), 'Thu', TO_DATE('19:30', 'HH24:MI'), 180);
INSERT INTO takesPlace (roomID, buildingID, semester, firstSession, lastSession, dayOfWeek, startTime, duration) VALUES(00001, 001, 'SS2013', TO_DATE('04.03.2013', 'DD.MM.YYYY'), TO_DATE('29.06.2013', 'DD.MM.YYYY'), 'Mon', TO_DATE('16:30', 'HH24:MI'), 90);


INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(1,'WS2013', 'lecture', 'Softwareengineering', TO_DATE('02.10.2013', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('19:30', 'HH24:MI'), 90, 87654321, 033521, 25, 00001, 001);
INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(2,'WS2013', 'lecture', 'Medienwissenschaft', TO_DATE('06.11.2013', 'DD.MM.YYYY'), TO_DATE('27.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('16:45', 'HH24:MI'), 90, 87654444, 033577, 100, 00001, 001);
INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(3,'WS2013', 'exam', 'Medienwissenschaft', TO_DATE('29.01.2014', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Wed', TO_DATE('16:45', 'HH24:MI'), 90, 87654444, 033577, 50, 00001, 001);
INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(4,'WS2013', 'lecture', 'Wissenschaftliches Arbeiten', TO_DATE('08.10.2013', 'DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Tue', TO_DATE('11:45', 'HH24:MI'), 120, 87654444, 033577, 250, 00001, 001);
INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(5,'WS2013', 'party', 'Weihnachtsfeier', TO_DATE('19.12.2013', 'DD.MM.YYYY'), TO_DATE('19.12.2013', 'DD.MM.YYYY'), 'Thu', TO_DATE('19:30', 'HH24:MI'), 180, 87654444, 033521, 350, 00001, 001);
INSERT INTO Event (eventID, semester,typ,name,firstSession, lastSession, dayOfWeek, startTime, duration, lecturerID, disciplineID, capacity, roomID, buildingID) VALUES(6,'SS2013', 'lecture', 'ADS', TO_DATE('04.03.2013', 'DD.MM.YYYY'), TO_DATE('29.06.2013', 'DD.MM.YYYY'), 'Mon', TO_DATE('16:30', 'HH24:MI'), 90, 87654321, 033521, 25, 00001, 001);


INSERT INTO Lecture VALUES('050052', 1, 'WS2013', 1, 'UE', TO_DATE('02.10.2013','DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Wed');
INSERT INTO Lecture VALUES('070532', 2, 'WS2013', 0, 'VO', TO_DATE('06.11.2013','DD.MM.YYYY'), TO_DATE('27.01.2014', 'DD.MM.YYYY'), 'Wed');
INSERT INTO Lecture VALUES('070618', 4, 'WS2013', 0, 'VO', TO_DATE('08.10.2013','DD.MM.YYYY'), TO_DATE('29.01.2014', 'DD.MM.YYYY'), 'Tue');
INSERT INTO Lecture VALUES('058271', 6, 'SS2013', 1, 'UE', TO_DATE('04.03.2013','DD.MM.YYYY'), TO_DATE('29.06.2013', 'DD.MM.YYYY'), 'Mon');

INSERT INTO Exam VALUES(3, 'WS2013', '070532', TO_DATE('29.01.2014', 'DD.MM.YYYY'));

INSERT INTO Attending VALUES('student1', 1, 'WS2013');
INSERT INTO Attending VALUES('student1', 6, 'SS2013');
INSERT INTO Attending VALUES('student2', 2, 'WS2013');
INSERT INTO Attending VALUES('student2', 3, 'WS2013');
INSERT INTO Attending VALUES('student2', 4, 'WS2013');

INSERT INTO Results VALUES(6, 'SS2013', '058271', TO_DATE('29.06.2013', 'DD.MM.YYYY'), '0876543', 2);