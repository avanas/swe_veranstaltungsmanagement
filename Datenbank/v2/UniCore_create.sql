CREATE TABLE Discipline (
	disciplineID	NUMBER(8),
	name			VARCHAR(25),
	PRIMARY KEY(disciplineID)
);

CREATE TABLE Building (
	buildingID		NUMBER(3),
	name			VARCHAR(30),
	plz				NUMBER(4) NOT NULL,
	street			VARCHAR(30) NOT NULL,
	nr				NUMBER(3),
	PRIMARY KEY(buildingID)
);

CREATE TABLE Room (
	roomID			NUMBER(5),
	buildingID		NUMBER(3),
	name			VARCHAR(30) NOT NULL,
	typ				VARCHAR(20),
	capacity		NUMBER(4),
	PRIMARY KEY(roomID,buildingID),
	CONSTRAINT fk_building FOREIGN KEY (buildingID) REFERENCES Building,
	CONSTRAINT capacityRoom CHECK (capacity > 0)
);

CREATE TABLE takesPlace (
	eventID			VARCHAR(8),
	roomID			NUMBER(5),
	buildingID		NUMBER(3),
	semester		VARCHAR(6),
	firstSession		DATE,
	lastSession		DATE,
	startTime		DATE,		
	duration		NUMERIC,
	PRIMARY KEY(eventID, roomID, buildingID),
	CONSTRAINT fk_roomTP FOREIGN KEY (roomID, buildingID) REFERENCES Room
);

CREATE TABLE Event (
	eventID			VARCHAR(8),
	semester		VARCHAR(6),
	typ			VARCHAR(20),
	name			VARCHAR(30),
	firstSession		DATE,
	lastSession		DATE,
	startTime		DATE,		
	duration		NUMERIC,
	lecturerID		NUMBER(8), 
	disciplineID		NUMBER(8),
	capacity		NUMERIC,
	roomID			NUMBER(5),
	buildingID		NUMBER(3),
	CONSTRAINT fk_roomEvent FOREIGN KEY (roomID, buildingID) REFERENCES Room,
	PRIMARY KEY(eventID)		
);


CREATE TABLE Lecture (
	LVNr			VARCHAR(8),
	eventID			VARCHAR(8),
	semester		VARCHAR(7),
	examimmanent	NUMBER(1),
	lectureType		VARCHAR(4),
	firstSession	DATE,
	lastSession		DATE,
	PRIMARY KEY(LVNr, semester),
	CONSTRAINT fk_eventLV FOREIGN KEY (eventID) REFERENCES Event
);

CREATE TABLE Exam (
	eventID			VARCHAR(8),
	semester		VARCHAR(7),	
	LVNr			VARCHAR(8),
	examDate		DATE,
	PRIMARY KEY(LVNr, examDate),
	CONSTRAINT fk_lvExam FOREIGN KEY (LVNr, semester) REFERENCES Lecture,
	CONSTRAINT fk_eventExam FOREIGN KEY (eventID) REFERENCES Event
);

CREATE TABLE Person (
	userName		VARCHAR(15),
	password		VARCHAR(15),
	firstName		VARCHAR(20),
	lastName		VARCHAR(20),
	email			VARCHAR(35) UNIQUE,
	birthdate		DATE,
	PRIMARY KEY(userName)
);

CREATE TABLE Student (
	userName		VARCHAR(15),
	matrNr			NUMBER(7),
	PRIMARY KEY(matrNr),
	CONSTRAINT fk_userStudent FOREIGN KEY (userName) REFERENCES Person
);

CREATE TABLE Results (
	eventID			VARCHAR(8),
	semester		VARCHAR(7) NOT NULL,	
	LVNr			VARCHAR(8),
	examDate		DATE,
	matrNr			NUMBER(7),
	grade			NUMBER(1),
	PRIMARY KEY(eventID, examDate, matrNr),
	CONSTRAINT fk_eventResult FOREIGN KEY (eventID) REFERENCES Event,
	CONSTRAINT fk_studentResult FOREIGN KEY (matrNr) REFERENCES Student,
	CONSTRAINT legalGrade CHECK (1<=grade AND 5>=grade)
);

CREATE TABLE Studies (
	matrNr			NUMBER(7),
	disciplineID		NUMBER(8),
	PRIMARY KEY(matrNr, disciplineID),
	CONSTRAINT fk_matrNrStudies FOREIGN KEY (matrNr) REFERENCES Student,
	CONSTRAINT fk_disciplineStudies FOREIGN KEY (disciplineID) REFERENCES Discipline
);

CREATE TABLE Lecturer (
	userName		VARCHAR(15),
	lecturerID		NUMBER(8),
	PRIMARY KEY(lecturerID),
	CONSTRAINT fk_userLecturer FOREIGN KEY (userName) REFERENCES Person
);
	
ALTER TABLE Event ADD CONSTRAINT fk_eventLecturer FOREIGN KEY (lecturerID) REFERENCES Lecturer;
ALTER TABLE Event ADD CONSTRAINT fk_TPevent FOREIGN KEY (eventID, roomID, buildingID) REFERENCES takesPlace;


CREATE TABLE Teaches (
	lecturerID		NUMBER(8),
	disciplineID	NUMBER(8),
	PRIMARY KEY(lecturerID, disciplineID),
	CONSTRAINT fk_lecIDTeach FOREIGN KEY (lecturerID) REFERENCES Lecturer,
	CONSTRAINT fk_discIDTeach FOREIGN KEY (disciplineID) REFERENCES Discipline
);
	
CREATE TABLE Statistician (
	userName		VARCHAR(15),
	statID			NUMBER(6),
	PRIMARY KEY(statID),
	CONSTRAINT fk_statistician FOREIGN KEY (userName) REFERENCES Person
);

CREATE TABLE Attending (
	userName		VARCHAR(15),
	eventID			VARCHAR(8),
	semester		VARCHAR(6) NOT NULL,
	PRIMARY KEY(userName,eventID),
	CONSTRAINT fk_eventAttend FOREIGN KEY (eventID) REFERENCES Event
);

CREATE SEQUENCE seq_eventID
	INCREMENT BY 1
	START WITH 1;

CREATE OR REPLACE TRIGGER trigger_eventID
	BEFORE INSERT ON takesPlace
	FOR EACH ROW
		DECLARE
			seq takesPlace.eventID%type;
		BEGIN
			SELECT seq_eventID.nextval INTO seq FROM dual;
			:new.eventID := seq;
		END;
		
/
